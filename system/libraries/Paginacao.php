<?php 
class Paginacao {

    /**
     * author: Wesley da Silva Pereira
     * email: wesley_cras@hotmail.com
     * 
     */
    
    function __construct() {
        parent::__construct();
        
        
    }

  public  function paginacao($totalPaginas,$por_pagina,$p){
        $p=0;
         $pg=$totalPaginas/$por_pagina;
       
        for ($i=1; $i < $pg; $i++) { 
              
         $pag[$i]='<a href="?p='.$i.'">['.$i.']';
     }
     return $pag;
     
      }

}