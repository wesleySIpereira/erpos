<?php
/**
 * ======================================
 * SUPPORT BOARD - CONFIG FILE
 * ======================================
 *
 * From this file you can set all the settings of the plugin.
 * Documentation at board.support/docs/php
 */

$sb_config_mysql = array(
    'host' => '',
    'username' => '',
    'password' => '',
    'db' => '',
);

$sb_config = array(
 'color-main' => '',
 'color-secondary' => '',
 'font-disable' => false,
 'rtl' => false,
 'auto-multilingual' => false,
 'agent-subtitle' => 'I\'m here for help you.',
 'desk-login' => true,
 'user-extra-1' => '',
 'user-extra-2' => '',
 'user-extra-3' => '',
 'user-extra-4' => '',
 'user-email' => true,
 'username-type' => 'email', //email,username
 'user-img' => true,
 'scrollbox-active' => false,
 'scrollbox-height' => '', //integer or "fullscreen"
 'scrollbox-offset' => '0',
 'scrollbox-options' => '',
 'hide-message-time' => false,
 'width' => '',
 'notify-agent-email' => true,
 'notify-user-email' => true,
 'push-notifications' => 'all', //all(default),users,agents
 'flash-notifications' => true,
 'flash-notifications-text' => '',
 'hide-chat-time' => true,
 'chat-visibility' => '', //all(default),logged
 'chat-sound' => true,
 'chat-avatars' => true,
 'welcome-active' => false,
 'welcome-always' => false,
 'welcome-msg' => '',
 'welcome-img' => '',
 'welcome-closed' => false,
 'follow-active' => true,
 'follow-msg' => '',
 'follow-fb' => '',
 'follow-wa' => '',
 'chat-title' => 'Support Board Chat',
 'chat-header' => 'We are an experienced team that provide fast and accurate responses to your questions.',
 'chat-header-avatars' => true,
 'chat-header-type' => '', //agents(default), brand
 'chat-brand-img' => '',
 'chat-header-img' => '',
 'chat-icon' => '',
 'slack-active' => false,
 'slack-token' => '',
 'slack-channel' => '',
 'bot-active' => false,
 'bot-token' => '',
 'bot-lan' => '',
 'bot-name' => '',
 'bot-img' => ''
);

$sb_config_email = array(
    'host' => '',
    'username' => '',
    'password' => '',
    'port' => '465',
    'SMTPSecure' => 'ssl',
    'email_from' => '',
    'email_subject_users' => '',
    'email_content_users' => '',
    'email_subject_agents' => '',
    'email_content_agents' => '',
);

?>
