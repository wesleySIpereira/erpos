<!doctype html>
<html class="fixed">

<head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title>ERPOS | <?php echo $titulo; ?></title>
    <meta name="keywords" content="ERPOS| Sistema de OS" />
    <meta name="description" content="Sistema de Ordem de serviço">
    <meta name="author" content="www.wepsistemas.net.eu.org">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url();?>assets/vendor/modernizr/modernizr.js"></script>
   
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/googleChart.js"></script>
    
    <!-- form validate -->


   

 
  
</head>

<body onload="msg()">
    <section class="body">

        <!-- start: header -->
        <header class="header">
            <div class="logo-container">
                <a href="../" class="logo">
                    <img src="<?php echo base_url();?>assets/images/logo.png" height="35" alt="Porto Admin" />
                </a>
                <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
                    <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>
            
            
            <!-- start: search & user box -->
            <div class="header-right">
           
            
                <span class="separator"></span>
               
						<div class="label label-info">
						 <b>Prefeitura Municipal de Patrocínio</b>
						
						</div>
					
                <span class="separator"></span>
                
                <div id="userbox" class="userbox">
                    <a href="#" data-toggle="dropdown">
                        <figure class="profile-picture">
                            <img src="assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
                        </figure>
                        <div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
                            <span class="name"><?php echo $usuario; ?></span>
                            <span class="role"><?php echo $cargo; ?></span>
                        </div>

                        <i class="fa custom-caret"></i>
                    </a>

                    <div class="dropdown-menu">
                        <ul class="list-unstyled">
                            <li class="divider"></li>
                            <li>
                                <a role="menuitem" tabindex="-1" href="pages-user-profile.html"><i class="fa fa-user"></i> Meus Dados</a>
                            </li>
                            <li>
                                <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a>
                            </li>
                            <li>
                                <a role="menuitem" tabindex="-1" href="<?php echo base_url();?>index.php/principal/logout"><i class="fa fa-power-off"></i> Sair</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end: search & user box -->
        </header>
        <!-- end: header -->

        <div class="inner-wrapper">
            <!-- start: sidebar -->

      <aside id="sidebar-left" class="sidebar-left">

                <div class="sidebar-header">
                   
                    <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>

                <div class="nano">
                    <div class="nano-content">
                        <nav id="menu" class="nav-main" role="navigation">
                        <?php  $nav=$this->uri->segment(1);?>
                            <ul class="nav nav-main">
                                <li class="<?php if($nav=='principal'){echo 'nav-expanded nav-active';};?>">
                                    <a href="<?php echo base_url();?>index.php">
                                        <i class="fa fa-home" aria-hidden="true"></i>
                                        <span>Dashboard</span>
                                    </a>
                                </li>
                                
                                <li class="nav-parent <?php if($nav=='secretaria' or $nav=='setor' or $nav=='cargo' or $nav =='funcionario'){echo 'nav-expanded nav-active';};?>">
                                    <a>
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <span>Cadastros</span>
                                    </a>
                                    <ul class="nav nav-children">
                                        <li  <?php if($nav=='secretaria'){ echo 'class="nav-active"'; } ?>>
                                        <a  href="<?php echo base_url();?>index.php/secretaria"> <i class="fa fa-angle-double-right" aria-hidden="true"></i>Secretaria</a>
                                        </li>
                                        <li <?php if($nav=='setor'){ echo 'class="nav-active"'; } ?>>
                                        <a href="<?php echo base_url();?>index.php/setor"><i class="fa   fa-angle-double-right" aria-hidden="true"></i>Setor</a>
                                        </li>
                                        <li <?php if($nav=='cargo'){ echo 'class="nav-active"'; } ?>>
                                        <a href="<?php echo base_url();?>index.php/cargo"><i class="fa   fa-angle-double-right" aria-hidden="true"></i>Cargo</a>
                                        </li>
                                        <li <?php if($nav=='funcionario'){ echo 'class="nav-active"'; } ?>>
                                        <a href="<?php echo base_url();?>index.php/funcionario"><i class="fa   fa-angle-double-right" aria-hidden="true"></i>Funcionário</a>
                                        </li>
                                       
                                    </ul>
                                </li>
                                <li class="nav-parent <?php if($nav=='tecnico'){echo 'nav-expanded nav-active';};?>">
                                    <a>
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <span>Equipe Técnica</span>
                                    </a>
                                    <ul class="nav nav-children">
                                        <li <?php if($nav=='tecnico'){ echo 'class="nav-active"'; } ?>  >
                                        <a href="<?php echo base_url();?>index.php/tecnico"> <i class="fa    fa-angle-double-right" aria-hidden="true"></i>Técnico</a>
                                        </li>
                                       
                                       
                                    </ul>
                                </li>
                                <li class="nav-parent <?php if(($nav=='equipamento')or ($nav=='cequipamento') or ($nav=='mequipamento') ){echo 'nav-expanded nav-active';};?>">
                                    <a>
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <span>Equipamentos</span>
                                    </a>
                                    <ul class="nav nav-children">
                                        <li <?php if($nav=='cequipamento') { echo 'class="nav-active"'; } ?>  >
                                        <a href="<?php echo base_url();?>index.php/cequipamento"> <i class="fa    fa-angle-double-right" aria-hidden="true"></i>Tipo Equipamento</a>
                                        </li>
                                        <li <?php if($nav=='mequipamento') { echo 'class="nav-active"'; } ?>  >
                                        <a href="<?php echo base_url();?>index.php/mequipamento"> <i class="fa    fa-angle-double-right" aria-hidden="true"></i>Marca equipamento</a>
                                        </li>
                                        <li <?php if($nav=='equipamento'){ echo 'class="nav-active"'; } ?>  >
                                        <a href="<?php echo base_url();?>index.php/equipamento"> <i class="fa    fa-angle-double-right" aria-hidden="true"></i>Equipamento</a>
                                        </li>
                                       
                                       
                                    </ul>
                                </li>
                                <li class="nav-parent <?php if($nav=='categoria' or $nav=='subcategoria' or $nav=='produto'){echo 'nav-expanded nav-active';};?>">
                                    <a>
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <span>Estoque</span>
                                    </a>
                                    <ul class="nav nav-children">
                                        <li <?php if($nav=='categoria'){ echo 'class="nav-active"'; } ?> >
                                        <a href="<?php echo base_url();?>index.php/categoria"> <i class="fa   fa-angle-double-right" aria-hidden="true"></i>Grupo</a>
                                        </li>
                                        <li <?php if($nav=='subcategoria'){ echo 'class="nav-active"'; } ?> >
                                        <a href="<?php echo base_url();?>index.php/subcategoria"><i class="fa   fa-angle-double-right" aria-hidden="true"></i>Sub Grupo</a>
                                        </li>
                                        <li <?php if($nav=='produto'){ echo 'class="nav-active"'; } ?> >
                                        <a href="<?php echo base_url();?>index.php/produto"><i class="fa   fa-angle-double-right" aria-hidden="true"></i>Produto</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-parent <?php if($nav=='os'){echo 'nav-expanded nav-active';};?> ">
                                    <a>
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <span>Chamados</span>
                                    </a>
                                    <ul class="nav nav-children">
                                        <li <?php if($nav=='os'){ echo 'class="nav-active"'; } ?>>
                                        <a href="<?php echo base_url();?>index.php/os"> <i class="fa   fa-angle-double-right" aria-hidden="true"></i>Chamados</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="<?php echo base_url();?>index.php/principal/logout">
                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                        <span>Sair</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>

                    </div>

                </div>

            </aside>
            <!-- end: sidebar -->

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2><?php echo $titulo; ?></h2>

                    <div class="right-wrapper pull-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.html">
                                    <i class="fa fa-home"></i>
                                </a>
                            </li>
                            <li><span><a href="<?php echo base_url('index.php/').$this->uri->segment(1);?>"><?php echo $this->uri->segment(1);?></a> </span></li>
                            <li><span><?php echo $titulo; ?></span></li>
                        </ol>

                        <a class="sidebar-right-toggle" ><i class="fa fa-chevron-left"></i></a>
                    </div>
                </header>

                <!-- start: page -->
			
				<?php
              
               //se existir a view passada chama caso
				if ((isset($view))){
				    
					$this->load->view($view);
					
				}else{
					
					$this->load->view('theme/404');
					
                }
               

				?>


                <!-- end: page -->
            </section>
        </div>

      
    </section>

		
	
		
	
	
		
		
		
		
		
		<script src="assets/javascripts/theme.js"></script>
		
	
		<script src="assets/javascripts/theme.custom.js"></script>
		
		
		<script src="assets/javascripts/theme.init.js"></script>


		
		<script src="assets/javascripts/forms/examples.validation.js"></script>
   
   <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.js"></script> 
    <script src="<?php echo base_url();?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/nanoscroller/nanoscroller.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/magnific-popup/magnific-popup.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
    <script src="<?php echo base_url();?>assets/javascripts/ui-elements/examples.modals.js"></script>
  
   <script src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
   <script src="<?php echo base_url();?>assets/js/form_validation.js"></script>
   <script src="<?php echo base_url();?>assets/dist/js/jquery.printElement.js"></script>
	
		
		
		

    <!-- Theme Base, Components and Settings -->
    <script src="<?php echo base_url();?>assets/javascripts/theme.js"></script> 

  
    <script src="<?php echo base_url();?>assets/javascripts/theme.custom.js"></script> 

 
    <script src="<?php echo base_url();?>assets/javascripts/theme.init.js"></script> 

 <!-- chama o script -->
 <?php if(isset($jscript)){
                   $this->load->view($jscript);
                    
                } ?>






</body>

</html>