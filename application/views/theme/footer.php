
<script type="text/javascript">
  //  $('#modal-success').modal('show');
     
		var base_url = '<?php echo base_url() ?>';
		
		function busca_setor(id_setor){
			
			$.post(base_url+"index.php/funcionario/get-setor", {
				cid_secretaria : id_setor
			}, function(data){
				$('#id_setor').html(data);
			});
    }
    
    function busca_funcionario(){
      var id_secretaria=$('#id_secretaria').val();
      var id_setor=$('#id_setor').val();
      var busca_nome=$('#busca_nome').val();
			$.post(base_url+"index.php/os/get-funcionario", {
        id_secretaria : id_secretaria,
        id_setor:id_setor,
        busca_nome:busca_nome
			}, function(data){
				$('#tb_funcionario').html(data);
			});
		}
    function logar(){
      var nm_funcionario=$('#user').val();
      var senha=$('#senha').val();
      //var id=id1;
     //passar todos os dados via ajax //#endregion
      var st_os=$('#st_os option:selected').val();
      var dt_atendimento=$('#dt_atendimento').val();
      var hr_atendimento=$('#hr_atendimento').val();
      var id_tecnico=$('#id_tecnico').val();
      var ld_tecnico=$('#ld_tecnico').val();
      var id_os=$('#id_os').val();
      var dt_encerramento=$('#dt_encerramento').val();
      
			$.post(base_url+"index.php/os/logar", {
        user : nm_funcionario,
        passwords:senha,
       // id2:id,
        st_os:st_os,
        dt_encerramento:dt_encerramento,
        dt_atendimento:dt_atendimento,
        hr_atendimento:hr_atendimento,
        id_tecnico:id_tecnico,
        ld_tecnico:ld_tecnico,
        id_os:id1
			}, function(data){
				$('#erro').html(data);
			});
		}
    // ajax filtro os //#endregion
    function os_busca_setor(id_setor){
			
			$.post(base_url+"index.php/os/get-setor", {
				cid_secretaria : id_setor
			}, function(data){
				$('#os_id_setor').html(data);
			});
    }
    
    //ajax filtro busca categoria
    function os_busca_categoria(id_setor){
			
			$.post(base_url+"index.php/categoria/get-categoria", {
				cid_secretaria : id_setor
			}, function(data){
				$('#os_id_setor').html(data);
			});
    }
   
    
    


	
 </script>
<script>
     function verificaUnicidade(){
      var nm_usuario=$('#nm_usuario').val();
      var mail_funcionario=$('#mail_funcionario').val()
      var nm_funcionario=$('#nome').val();
     
      
			$.post(base_url+"index.php/funcionario/verificaUnicidade", {
        nm_usuario:nm_usuario,
        nm_funcionario:nm_funcionario,
        mail_funcionario:mail_funcionario        
        
			}, function(data){
                 var retorno=data;
                
                 if(retorno==1){
                    $('#modal-danger').hide()
                   
                 }
                 if(retorno==0){
                    $('#botao').attr('disabled','true'); 
                    $('.modal-body').html('<p>Esse Usuário já está cadastrado!</p><p>Por segurança o botão salvar foi desativado,recomece o processo de cadastro verificando os dados.</p>');
                    $('#modal-danger').modal();
                       
                   
                 }
			});
		}
        function verificaEmailExiste(){
      
      var mail_funcionario=$('#mail_funcionario').val()
      
     
      
			$.post(base_url+"index.php/funcionario/verificaEmailExiste", {
     
        mail_funcionario:mail_funcionario        
        
			}, function(data){
                 var retorno=data;
                
                 if(retorno==1){
                    $('#modal-danger').hide()
                    $('.Erro-email').addClass('hide'); 
                 }
                 if(retorno==0){
                    $('#mail_funcionario').val(''); 
                    $('#mail_funcionario').focus();
                   
                    $('#call-error').trigger('click');
                    $('.Erro-email').html('O e-mail <b>'+mail_funcionario+'</b> já esta sendo usado. Por favor entre com um email válido!');
                    $('.Erro-email').removeClass('hide');  
                   
                 }
			});
		}
        function verificaNumeroResp(){
      
      var n_resp=$('#n_resp').val()
      
     
      
			$.post(base_url+"index.php/funcionario/verificaNumeroResp", {
     
           n_resp:n_resp        
        
			}, function(data){
                 var retorno=data;
                
                 if(retorno==1){
                    $('#modal-danger').hide()
                    $('.Erro-resp').addClass('hide'); 
                 }
                 if(retorno==0){
                    $('#n_resp').val(''); 
                    $('#n_resp').focus();
                    $('.Erro-resp').html('Esse Resp nº <b>'+n_resp+'</b> já esta sendo usado.Entre com seu número. Duvida entre em contato com CPD ramal:476-213!');
                    $('.Erro-resp').removeClass('hide');  
                       
                   
                 }
			});
		}
        function verificaUsuario(){
      
      var nm_usuario=$('#nm_usuario').val()
      
     
      
			$.post(base_url+"index.php/funcionario/verificaUsuario", {
     
           nm_usuario:nm_usuario      
        
			}, function(data){
                 var retorno=data;
                
                 if(retorno==1){
                    $('#modal-danger').hide()
                    $('.Erro-usuario').addClass('hide');
                 }
                 if(retorno==0){
                    $('#nm_usuario').val(''); 
                    $('#nm_usuario').focus();
                    $('.Erro-usuario').html('O  nome de usuário <b>'+nm_usuario+'</b> já esta sendo usado.Sugerimos que utilize o padão: joão alves pereira, joaoap. Dúvidas ramais 476-213');
                    $('.Erro-usuario').removeClass('hide'); 
                       
                   
                 }
			});
		}    
</script> 

<!-- Main Footer -->
  <!-- treta aqui validar com js 08-05-2019 -->
<div id="modalHeaderColorPrimary" class="modal-block modal-header-color modal-block-primary mfp-hide" >
										<section class="panel">
											<header class="panel-heading">
												<h2 class="panel-title">Are you sure?</h2>
											</header>
											<div class="panel-body">
												<div class="modal-wrapper">
													<div class="modal-icon">
														<i class="fa fa-question-circle"></i>
													</div>
													<div class="modal-text">
														<h4>Primary</h4>
														<p>Are you sure that you want to delete this image?</p>
													</div>
												</div>
											</div>
											<footer class="panel-footer">
												<div class="row">
													<div class="col-md-12 text-right">
														<button class="btn btn-primary modal-confirm">Confirm</button>
														<button class="btn btn-default modal-dismiss">Cancel</button>
													</div>
												</div>
											</footer>
										</section>
									</div>
<!--modal erro caso usuario já esteja cadastrado -->
<div class="modal modal-open modal-warning fade" id="modal-danger" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">ERPOS</h4>
              </div>
              <div class="modal-body default">
                <p>O usuário já está cadastrado! por favor verifique.&hellip;</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Entendi</button>
               
              </div>
            </div>
          </div>  
</div>     
            <!-- /.modal-content -->
