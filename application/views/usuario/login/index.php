<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>ERPOS - Login</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url();?>assets/dist/css/all.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/font-awesome.css">
  <!-- Custom styles for this template-->
  <link href="<?php echo base_url();?>assets/dist/css/sb-admin.css" rel="stylesheet">

</head>
<style>
     #carregando div,#carregando-nfc div {
        border-color: #f3f3f3;
        border-top-color: #0C1B25;
        border-bottom-color: #0C1B25;
    }

    #bem-vindo .modal-header, #curtir-facebook .modal-header{
        background: #0C1B25;
        color: #FFFFFF;
    }

    .pace .pace-progress {
      background: #1F4968;
    }

    a {
      color: #0C1B25;
    }

    a:hover,
    a:active,
    a:focus {
      color: #1F4968    }

    .text-primary{
        color: #0C1B25    }

    .text-primary:hover{
        color: #1F4968    }

    .cor-padrao{
      color: #0C1B25    }

    .flash-tab{
      background: #0C1B25;
    }

    .dropdown-menu > li > a:hover {
      background-color: #0C1B25;
    }

    .navbar-nav > .user-menu > .dropdown-menu > li.user-header {
      background: #0C1B25;
    }

    .box.box-primary {
      border-top-color: #0C1B25;
    }

    .box.box-solid.box-primary > .box-header {
      background: #0C1B25;
      background-color: #0C1B25;
    }

    .box .todo-list > li.primary {
      border-left-color: #0C1B25;
    }

    .bg-light-blue {
      background-color: #0C1B25 !important;
    }

    .bg-olive {
      background-color: #0C1B25 !important;
    }

    .text-light-blue {
      color: #0C1B25 !important;
    }

    .text-olive {
      color: #0C1B25 !important;
    }

    .btn.btn-primary {
      background-color: #0C1B25;
      border-color: #1F4968;
    }

    .btn-primary:hover,
    .btn-primary:focus,
    .btn-primary:active,
    .btn-primary.active,
    .open .dropdown-toggle.btn-primary {
      background-color: #1F4968;
      border-color: #0C1B25;
    }

    .nav.nav-pills > li.active > a,
    .nav.nav-pills > li.active > a:hover {
      border-top-color: #0C1B25;
      color: #444;
    }

    .nav.nav-pills.nav-stacked > li.active > a,
    .nav.nav-pills.nav-stacked > li.active > a:hover {
      border-left-color: #0C1B25;
    }

    .nav-tabs-custom > .nav-tabs > li.active {
      border-top-color: #0C1B25;
    }

    .form-box .header {
      background: #0C1B25;
    }

    .skin-blue .navbar {
      background-color: #0C1B25;
    }

    .form-control:hover,.form-control:focus,.form-control-table:hover,.form-control-table:focus{
      border-color: #0C1B25 !important;
    }

    .progress-bar-light-blue,
    .progress-bar-primary {
      background-color: #0C1B25;
    }

    fieldset legend {
        color: #0C1B25;
    }

    table.calendario tr td .compromissos{
        background: #0C1B25;
    }

    .btn.btn-primary {
      border-color: #0C1B25;
    }

    .btn.btn-primary:hover,
    .btn.btn-primary:active,
    .btn.btn-primary.hover {
      background-color: #0C1B25;
    }

    .skin-blue .logo {
      background-color: #0C1B25;
    }


    div.token-input-dropdown ul li.token-input-selected-dropdown-item {
        background: #0C1B25 !important;
    }

    .text-info {
      color:#0C1B25;
    }
    .text-info:hover {
      color:#1F4968;
    }

    .pagination > .active > a,
    .pagination > .active > span,
    .pagination > .active > a:hover,
    .pagination > .active > span:hover,
    .pagination > .active > a:focus,
    .pagination > .active > span:focus {
      background-color: #0C1B25;
    }

    .wizard li.active span.round-tab {
        border: 2px solid #0C1B25;
    }

    .wizard li.active span.round-tab i{
        color: #0C1B25;
    }

    .wizard li:after{
        border-bottom-color: #0C1B25;
    }

    .wizard li.active:after {
        border-bottom-color: #0C1B25;
    }

    .ui-button.ui-state-active:hover {
    	background: #0C1B25 !important;
    }
</style> 
<!--codigo para imprimir via javascritp -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn-imprimir").click(function() {
            $('#tab1').printElement();
        });
    });
</script>

<body style="background-color:#7a868f;">

  <div style="padding:0%; margin: 10%;" class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header text-bold text-info text-center">ERPOS | Login</div>
      <div class="card-body">
        <form action="<?php echo base_url()?>index.php/usuario/logar" method="post">
          <div class="form-group">
          
            <div class="form-group">
              <input type="text"class="form-control" name="nm_usuario" id="nm_usuario" placeholder="Usuário" >
             
            </div>
          </div>
          <div class="form-group">
          
            <div class="form-group">
              <input type="password" id="senha" name="senha" class="form-control" placeholder="Senha" >
              
            </div>
          </div>
        
          <input type="submit" class="btn btn-info btn-block" href="index.html" value="Entrar">
        </form>
       
        <?php
        echo '<hr>';
        get_msg('salvo');
        echo '<br>';
        ?>
    
        <div class="text-center">
          <a class="d-block small mt-3" href="<?php echo base_url()?>index.php/usuario/registrar">Criar nova conta</a>
          <a class="d-block small" href="#">Esqueci minha senha</a>
        </div>
      </div>
    </div>
  </div>

  

</body>

</html>

