<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>ERPOS | Cadastrar-me</title>

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->

  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/square/blue.css">




  <!--Form validation -->
  <script src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/form_validation.js"></script>



  <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/matrix-media.css"> -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/matrix-media.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/matrix-style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/font-awesome.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/jquery-ui-1.9.2.custom.css">
  <!--full  calendar -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/dist/css/fullcalendar.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/dist/css/jquery-ui.css" />

  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/select2/dist/css/select2.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->



</head>
<style>
     #carregando div,#carregando-nfc div {
        border-color: #f3f3f3;
        border-top-color: #0C1B25;
        border-bottom-color: #0C1B25;
    }

    #bem-vindo .modal-header, #curtir-facebook .modal-header{
        background: #0C1B25;
        color: #FFFFFF;
    }

    .pace .pace-progress {
      background: #1F4968;
    }

    a {
      color: #0C1B25;
    }

    a:hover,
    a:active,
    a:focus {
      color: #1F4968    }

    .text-primary{
        color: #0C1B25    }

    .text-primary:hover{
        color: #1F4968    }

    .cor-padrao{
      color: #0C1B25    }

    .flash-tab{
      background: #0C1B25;
    }

    .dropdown-menu > li > a:hover {
      background-color: #0C1B25;
    }

    .navbar-nav > .user-menu > .dropdown-menu > li.user-header {
      background: #0C1B25;
    }

    .box.box-primary {
      border-top-color: #0C1B25;
    }

    .box.box-solid.box-primary > .box-header {
      background: #0C1B25;
      background-color: #0C1B25;
    }

    .box .todo-list > li.primary {
      border-left-color: #0C1B25;
    }

    .bg-light-blue {
      background-color: #0C1B25 !important;
    }

    .bg-olive {
      background-color: #0C1B25 !important;
    }

    .text-light-blue {
      color: #0C1B25 !important;
    }

    .text-olive {
      color: #0C1B25 !important;
    }

    .btn.btn-primary {
      background-color: #0C1B25;
      border-color: #1F4968;
    }

    .btn-primary:hover,
    .btn-primary:focus,
    .btn-primary:active,
    .btn-primary.active,
    .open .dropdown-toggle.btn-primary {
      background-color: #1F4968;
      border-color: #0C1B25;
    }

    .nav.nav-pills > li.active > a,
    .nav.nav-pills > li.active > a:hover {
      border-top-color: #0C1B25;
      color: #444;
    }

    .nav.nav-pills.nav-stacked > li.active > a,
    .nav.nav-pills.nav-stacked > li.active > a:hover {
      border-left-color: #0C1B25;
    }

    .nav-tabs-custom > .nav-tabs > li.active {
      border-top-color: #0C1B25;
    }

    .form-box .header {
      background: #0C1B25;
    }

    .skin-blue .navbar {
      background-color: #0C1B25;
    }

    .form-control:hover,.form-control:focus,.form-control-table:hover,.form-control-table:focus{
      border-color: #0C1B25 !important;
    }

    .progress-bar-light-blue,
    .progress-bar-primary {
      background-color: #0C1B25;
    }

    fieldset legend {
        color: #0C1B25;
    }

    table.calendario tr td .compromissos{
        background: #0C1B25;
    }

    .btn.btn-primary {
      border-color: #0C1B25;
    }

    .btn.btn-primary:hover,
    .btn.btn-primary:active,
    .btn.btn-primary.hover {
      background-color: #0C1B25;
    }

    .skin-blue .logo {
      background-color: #0C1B25;
    }


    div.token-input-dropdown ul li.token-input-selected-dropdown-item {
        background: #0C1B25 !important;
    }

    .text-info {
      color:#0C1B25;
    }
    .text-info:hover {
      color:#1F4968;
    }

    .pagination > .active > a,
    .pagination > .active > span,
    .pagination > .active > a:hover,
    .pagination > .active > span:hover,
    .pagination > .active > a:focus,
    .pagination > .active > span:focus {
      background-color: #0C1B25;
    }

    .wizard li.active span.round-tab {
        border: 2px solid #0C1B25;
    }

    .wizard li.active span.round-tab i{
        color: #0C1B25;
    }

    .wizard li:after{
        border-bottom-color: #0C1B25;
    }

    .wizard li.active:after {
        border-bottom-color: #0C1B25;
    }

    .ui-button.ui-state-active:hover {
    	background: #0C1B25 !important;
    }
</style> 
<!--codigo para imprimir via javascritp -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn-imprimir").click(function() {
            $('#tab1').printElement();
        });
    });
</script>

<body style="background-color: #7a868f;">

  <div class="register-box  ">
    

    <div class=" ">
    <div class="span12">
        <div class="widget-box">
          <div class="widget-title text-center"> <span class="icon"> <i class="icon-list"></i> </span>
            <h5 class="text-center">ERPOS | <j class="text-white">          Criar minha conta.</j></h5>
          </div>
          <div class="widget-content"> 
          


<form action="<?php echo base_url() . 'index.php/usuario/save'; ?>" id="form_funcionario" method="post">
  <div class="form-group  has-feedback">
    <label>NOME</label>
    <input type="text" class="form-control   " id="nome" name="nome">

  </div>


  <div class="form-group  has-feedback">
    <label>E-mail</label>
    <input type="email" required class="form-control   " id="mail_funcionario" onchange="verificaEmailExiste()" name="mail_funcionario" placeholder="E-mail">

  </div>
  <div class="form-group has-feedback">
    <label>Nº RESP</label>
    <input type="text" class="form-control   " id="n_resp" name="n_resp" onchange="verificaNumeroResp()" placeholder="Nº RESP">

  </div>
  <div class="form-group has-feedback">
    <label>Cargo</label>
    <select class="form-control select2   " id="cid_cargo" name="cid_cargo" style="width: 100%;">
      <option></option>
      <?php if ($cargo) {
        foreach ($cargo as $cargos) { ?>
          <option value="<?php echo $cargos->id_cargo; ?>"><?php echo $cargos->nm_cargo; ?></option>
        <?php
      }
    } ?>
    </select>
  </div>
  <div class="form-group has-feedback">
    <label>Secretaria</label>
    <select class="form-control select2   " id="id_secretaria" name="cid_secretaria" onchange="busca_setor($(this).val())" style="width: 100%;">
      <option></option>
      <?php if ($secretaria) {
        foreach ($secretaria as $secretarias) { ?>
          <option value="<?php echo $secretarias->id_secretaria; ?>"><?php echo $secretarias->nm_secretaria; ?></option>
        <?php
      }
    } ?>
    </select>
  </div>
  <div class="form-group has-feedback">
    <label>Setor</label>
    <select class="form-control select2   " id="id_setor" name="id_setor" style="width: 100%;">


    </select>
  </div>
  <div class="form-group has-feedback">
    <label for="exampleInputEmail1">Local</label>

    <input type="text" class="form-control   " id="nm_local" name="nm_local">


  </div>
  <div class="form-group has-feedback">
    <label for="exampleInputEmail1">Ramal/Telefone</label>

    <input type="text" class="form-control   " id="n_ramal" name="n_ramal">



  </div>
  <div class="form-group has-feedback">
    <label for="exampleInputEmail1">Obs. sobre local de trabalho</label>

    <input type="text" class="form-control   " id="obs_local" name="obs_local">

  </div>
  <div class="widget-box">
          <div class="widget-title text-center"> <span class="icon"> <i class="icon-list"></i> </span>
            <h5 class="text-center">ERPOS | <j class="text-white">       Dados de Acesso</j></h5>
          </div>
          <div class="widget-content"> 
  
  <div class="form-group has-feedback">
    <label for="exampleInputEmail1">Usuário</label>

    <input type="text" class="form-control " onchange="verificaUsuario()" id="nm_usuario" name="nm_usuario">
  </div>
  <div class="form-group has-feedback">
    <label for="exampleInputEmail1">Senha</label>

    <input type="password" class="form-control" id="senha" name="senha">
  </div>
  <div class="form-group has-feedback">
    <label for="exampleInputEmail1">Repita senha</label>

    <input type="password" class="form-control" id="confirma_senha" name="confirma_senha">
  </div>
  <div class="row text-center">
  </div>
          </div>
  </div>
         
    <div class="col-xs-5 ">
      <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-check-square"> </i> Enviar</button>
    </div>
  
    <!-- /.col -->
  </div>
</form>

<br>

<a href="<?php echo base_url() ?>index.php/usuario" class="btn btn-sx btn-link"><i class="fa fa-user"> </i> já tenho cadastro.</a>

<p class="alert">
              <strong>Aviso!</strong> Qualquer dúvida ou dificuldade entre em contato com CPD ramais: 476,500 ou 213. </p>

       
          </div>
        </div>
      </div>
    
      </div> 



    <!-- /.form-box -->
  
  <!-- /.register-box -->
 









  <!--modal erro caso usuario já esteja cadastrado -->
  <div class="modal modal-warning fade" id="modal-danger" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">ERPOS</h4>
        </div>
        <div class="modal-body text-black">
          <p>O usuário já está cadastrado! por favor verifique.&hellip;</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Entendi</button>

        </div>
      </div>
    </div>
  </div>
  
  <!-- /.modal-content -->
  <!--modal nova marca -->

  <script>
    function verificaUnicidade() {
      var nm_usuario = $('#nm_usuario').val();
      var mail_funcionario = $('#mail_funcionario').val()
      var nm_funcionario = $('#nome').val();


      $.post(base_url + "index.php/funcionario/verificaUnicidade", {
        nm_usuario: nm_usuario,
        nm_funcionario: nm_funcionario,
        mail_funcionario: mail_funcionario

      }, function(data) {
        var retorno = data;

        if (retorno == 1) {
          $('#modal-danger').hide()

        }
        if (retorno == 0) {
          $('#botao').attr('disabled', 'true');
          $('.modal-body').html('<p class="text-black">Esse Usuário já está cadastrado!</p><p>Por segurança o botão salvar foi desativado,recomece o processo de cadastro verificando os dados.</p>');
          $('#modal-danger').modal();


        }
      });
    }

    function verificaEmailExiste() {

      var mail_funcionario = $('#mail_funcionario').val()



      $.post(base_url + "index.php/funcionario/verificaEmailExiste", {

        mail_funcionario: mail_funcionario

      }, function(data) {
        var retorno = data;

        if (retorno == 1) {
          $('#modal-danger').hide()

        }
        if (retorno == 0) {
          $('#mail_funcionario').val('');
          $('#mail_funcionario').focus();
          $('.modal-body').html('<p>O e-mail:<strong> ' + mail_funcionario + '</strong> já está em uso.</p><p>Entre com um novo endereço de e-mail</p>');
          $('#modal-danger').modal();


        }
      });
    }

    function verificaNumeroResp() {

      var n_resp = $('#n_resp').val()



      $.post(base_url + "index.php/funcionario/verificaNumeroResp", {

        n_resp: n_resp

      }, function(data) {
        var retorno = data;

        if (retorno == 1) {
          $('#modal-danger').hide()

        }
        if (retorno == 0) {
          $('#n_resp').val('');
          $('#n_resp').focus();
          $('.modal-body').html('<p>O Resp :<strong> ' + n_resp + '</strong> já está em uso.</p><p>O Número Resp é único a cada funcionário caso não  saiba ou não se lembre, ligue no departamento de RH.</p>');
          $('#modal-danger').modal();


        }
      });
    }

    function verificaUsuario() {

      var nm_usuario = $('#nm_usuario').val()



      $.post(base_url + "index.php/funcionario/verificaUsuario", {

        nm_usuario: nm_usuario

      }, function(data) {
        var retorno = data;

        if (retorno == 1) {
          $('#modal-danger').hide()

        }
        if (retorno == 0) {
          $('#nm_usuario').val('');
          $('#nm_usuario').focus();
          $('.modal-body').html('<p>O nome de usuário:<strong> ' + nm_usuario + '</strong> já está em uso.</p><p>Para usuário sugerimos que adote o mesmo usado no sistema visão.</p></p>Exemplo: João Alves Lima, resultado: joaoal<p>');
          $('#modal-danger').modal();


        }
      });
    }
  </script>






  <!-- Bootstrap 3.3.7 -->
  <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
 

  <script type="text/javascript">
    var base_url = '<?php echo base_url() ?>';

    function busca_setor(id_setor) {

      $.post(base_url + "index.php/funcionario/get-setor", {
        cid_secretaria: id_setor
      }, function(data) {
        $('#id_setor').html(data);
      });
    }

    function busca_funcionario() {
      var id_secretaria = $('#id_secretaria').val();
      var id_setor = $('#id_setor').val();
      var busca_nome = $('#busca_nome').val();
      $.post(base_url + "index.php/os/get-funcionario", {
        id_secretaria: id_secretaria,
        id_setor: id_setor,
        busca_nome: busca_nome
      }, function(data) {
        $('#tb_funcionario').html(data);
      });
    }

    function logar() {
      var nm_funcionario = $('#user').val();
      var senha = $('#senha').val();
      //var id=id1;
      //passar todos os dados via ajax //#endregion
      var st_os = $('#st_os option:selected').val();
      var dt_atendimento = $('#dt_atendimento').val();
      var hr_atendimento = $('#hr_atendimento').val();
      var id_tecnico = $('#id_tecnico').val();
      var ld_tecnico = $('#ld_tecnico').val();
      var id_os = $('#id_os').val();
      var dt_encerramento = $('#dt_encerramento').val();

      $.post(base_url + "index.php/os/logar", {
        user: nm_funcionario,
        passwords: senha,
        // id2:id,
        st_os: st_os,
        dt_encerramento: dt_encerramento,
        dt_atendimento: dt_atendimento,
        hr_atendimento: hr_atendimento,
        id_tecnico: id_tecnico,
        ld_tecnico: ld_tecnico,
        id_os: id1
      }, function(data) {
        $('#erro').html(data);
      });
    }
    // ajax filtro os //#endregion
    function os_busca_setor(id_setor) {

      $.post(base_url + "index.php/os/get-setor", {
        cid_secretaria: id_setor
      }, function(data) {
        $('#os_id_setor').html(data);
      });
    }
  </script>
</body>

</html>