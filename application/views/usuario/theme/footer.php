
<!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
     Sistema de Chamados -ERPOS
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo date('Y'); ?>  <a href="http://wepsistemas.net.eu.org" target="blank">WEP-sistemas</a>.</strong> Inovando sempre.
  </footer>


<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->



<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/dist/js/adminlte.min.js"></script>
<!-- Select2 -->
<!-- <script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script> -->
<!-- Form validation -->




<!--matrix js -->


<!-- <script src="<?php echo base_url();?>assets/dist/js/jquery-ui.js"></script> -->
<script src="<?php echo base_url();?>assets/js/jquery.mask.js"></script>
<script src="<?php echo base_url();?>assets/dist/js/mascaras.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
     <script type="text/javascript">


		var base_url = '<?php echo base_url() ?>';
		
		function busca_setor(id_setor){
			
			$.post(base_url+"index.php/funcionario/get-setor", {
				cid_secretaria : id_setor
			}, function(data){
				$('#id_setor').html(data);
			});
    }
    
    function busca_funcionario(){
      var id_secretaria=$('#id_secretaria').val();
      var id_setor=$('#id_setor').val();
      var busca_nome=$('#busca_nome').val();
			$.post(base_url+"index.php/os/get-funcionario", {
        id_secretaria : id_secretaria,
        id_setor:id_setor,
        busca_nome:busca_nome
			}, function(data){
				$('#tb_funcionario').html(data);
			});
		}
    function logar(){
      var nm_funcionario=$('#user').val();
      var senha=$('#senha').val();
      //var id=id1;
     //passar todos os dados via ajax //#endregion
      var st_os=$('#st_os option:selected').val();
      var dt_atendimento=$('#dt_atendimento').val();
      var hr_atendimento=$('#hr_atendimento').val();
      var id_tecnico=$('#id_tecnico').val();
      var ld_tecnico=$('#ld_tecnico').val();
      var id_os=$('#id_os').val();
      var dt_encerramento=$('#dt_encerramento').val();
      
			$.post(base_url+"index.php/os/logar", {
        user : nm_funcionario,
        passwords:senha,
       // id2:id,
        st_os:st_os,
        dt_encerramento:dt_encerramento,
        dt_atendimento:dt_atendimento,
        hr_atendimento:hr_atendimento,
        id_tecnico:id_tecnico,
        ld_tecnico:ld_tecnico,
        id_os:id1
			}, function(data){
				$('#erro').html(data);
			});
		}
    // ajax filtro os //#endregion
    function os_busca_setor(id_setor){
			
			$.post(base_url+"index.php/os/get-setor", {
				cid_secretaria : id_setor
			}, function(data){
				$('#os_id_setor').html(data);
			});
    }
    
   
    
    


	
  </script>
 
</body>
</html>

