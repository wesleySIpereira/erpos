<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Categoria
            <small>Editar categoria</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Dasboard</a></li>
            <li class="active"><?php if ($this->uri->segment(1) != null) { ?><a href="<?php echo base_url() . 'index.php/' . $this->uri->segment(1) ?>" class="tip-bottom" title="<?php echo ucfirst($this->uri->segment(1)); ?>"> <?php echo ucfirst($this->uri->segment(1)); ?></a><?php }; ?>  </li>
        </ol>
    </section>





    <section class="content">
    <?php
        get_msg('salvo');
        ?>
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">

                <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Editar categoria</h5>
          </div>
          <form role="form" method="post" id="formUsuarios" action="<?php echo base_url() . 'index.php/categoria/update'; ?>">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nome</label>

                                    <input type="text" class="form-control     " id="txtnome" name="nm_categoria" value="<?php echo $categoria[0]->nm_categoria; ?>" placeholder="Nome.">
                                    <?php echo form_error('nome', '<div class="text-danger">', '</div>'); ?>
                                </div>
                                <input type="hidden" name="id_categoria" value="<?php echo $categoria[0]->id_categoria; ?>">
                                   
                                

                                <!-- /.box-body -->

                                <div class="box-footer text-right">
                                    <button type="submit" class="btn btn-primary "><i class="fa fa-check"></i> Atualizar</button>
                                </div>
                        </form>
         
        </div>
      </div>
                </div>
                </div>

                </section>

            


            </div>

            <!-- /.content-wrapper -->




          