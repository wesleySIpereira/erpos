<div class="row">
    <div class="col-md-12">
        <div class="tabs">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#popular" data-toggle="tab" aria-expanded="true"> Dados gerais</a>
                </li>
                <li class="">
                    <a href="#recent" data-toggle="tab" aria-expanded="false"> Historico</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="popular" class="tab-pane active">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td style="text-align: right; width: 11%"><strong> <b>Nº Chamado:</b></strong></td>
                                <td colspan="8"><?php echo $os[0]->id_os; ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: right"><strong><b>Data chamado: </b></strong></td>
                                <td><?php echo formataVisao($os[0]->dt_os); ?></td>
                                <td style="text-align: right;"><b>Horário: </b></td>
                                <td colspan="5"><?php echo $os[0]->hr_os; ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: right"><strong><b>Funcionário: </b></strong></td>
                                <td colspan="8"><?php echo $os[0]->nm_funcionario; ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: right"><strong><b>Secretaria: </b></strong></td>
                                <td colspan=""><?php echo $os[0]->nm_secretaria; ?></td>
                                <td style="text-align: right"><strong><b>Setor: </b></strong></td>
                                <td colspan=""><?php echo $os[0]->nm_setor; ?></td>
                                <td style="text-align: right"><strong><b>Local: </b></strong></td>
                                <td colspan=""><?php echo $os[0]->nm_local; ?></td>
                                <td style="text-align: right"><strong><b>Ramal: </b></strong></td>
                                <td colspan=""><?php echo $os[0]->n_ramal; ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: right"><strong><b>Título: </b></strong></td>
                                <td colspan="8"><?php echo $os[0]->titulo_os; ?></td>
                            </tr>
                            <?php //pesquisa para saber quem foi o filha da puta que atendeu essa porra p/ passar informação p/ cliente pau no cu
                            $id_tecnico = $os[0]->id_tecnico;
                            $like = array(
                                'id_tecnico' => $id_tecnico
                            );
                            $tecnico = $this->Mos->listaOsTecnicoLike($p = 0, $por_pagina = null, 'v_tecnico', '*', $like, $porData = null, $order = null);   ?>
                            <tr>
                                <td style="text-align: right"><strong> <b>Problema: </b></strong></td>
                                <td colspan="8"><?php echo $os[0]->df_os; ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: right"><strong> <b>Observação: </b></strong></td>
                                <td colspan="8"><?php echo $os[0]->ob_os; ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: right; width: 13%"><strong>Status:</strong></td>
                                <td colspan="7"><?php if ($os[0]->st_os == 'Fechado') {
                                                    echo '<div class="badge " style="background-color: green; border-color: #E97F02"> Finalizado </div>';
                                                }
                                                if ($os[0]->st_os == 'Cancelado') {
                                                    echo '<div class="badge"  style="background-color: dark ; padding:auto; border-color: #E97F02">' . $os[0]->st_os . '</div>';
                                                }
                                                if ($os[0]->st_os == 'Aberto') {
                                                    echo '<div  class="badge info" style="background-color: #8A9B0F; border-color: #8A9B0F">' . $os[0]->st_os . '</div>';
                                                }
                                                if ($os[0]->st_os == 'Resolvendo') {
                                                    echo '<div class="badge badge-info" >' . $os[0]->st_os . '</div>';
                                                }
                                                if ($os[0]->st_os == 'Parado') {
                                                    echo '<div class="badge" style="background-color: #CDB380; border-color: #CDB380" >' . $os[0]->st_os . '</div>';
                                                }
                                                ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: left;"><strong>Data Atendimento:</strong></td>
                                <td><?php if (isset($os[0]->dt_atendimento)) {
                                        echo formataVisao($os[0]->dt_atendimento);
                                    } else {
                                        echo 'Aguardando...';
                                    } ?> </td>

                                <td style="text-align: right"><strong>Horário:</strong></td>
                                <td colspan="1"><?php if (isset($os[0]->hr_atendimento)) {
                                                    echo $os[0]->hr_atendimento;
                                                } else {
                                                    echo 'Aguardando...';
                                                } ?></td>
                                <td colspan="" style="text-align: right;"><strong>Técnico Responsável:</strong></td>
                                <td colspan="3"> <?php if ($tecnico['0']->id_tecnico == 1) {
                                                        echo 'Aguardando Atendimento...';
                                                    } else {
                                                        echo   $tecnico['0']->nm_funcionario;
                                                    } ?></td>
                            </tr>

                            <tr>
                                <td style="text-align: right;"><strong>Solução:</strong></td>
                                <td colspan="8"><?php echo $os[0]->ld_tecnico; ?></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <div id="recent" class="tab-pane">
                    <table class="table table-bordered table-condensed">
                        <thead>
                            <th width="7%" style="text-align: center;"><strong>Data Atendimento</strong></th>
                            <th width="1%"><strong>Status:</strong></th>
                            <th width="40%" style="text-align: center;"><strong>Solução</strong></th>
                            <th width="10%" style="text-align: center;"><strong>Técnico Responsável</strong></th>

                        </thead>
                        <?php if (isset($movimentacao)) {
                            foreach ($movimentacao as $movi) { ?>

                                <tr>
                                    <td style="text-align:center;"><?php if (isset($movi->dt_movimentacao)) {
                                                                        echo formataVisao($movi->dt_movimentacao);
                                                                    } else {
                                                                        echo 'Aguardando...';
                                                                    } ?>


                                        <?php if (isset($movi->hr_movimentacao)) {
                                            echo '- ' . $movi->hr_movimentacao;
                                        } else {
                                            echo 'Aguardando...';
                                        } ?></td>

                                    <td style="text-align:center;"><?php if ($movi->st_os == 'Fechado') {
                                                                        echo '<div class="badge " style="background-color: green; border-color: #E97F02"> Finalizado </div>';
                                                                    }
                                                                    if ($movi->st_os == 'Cancelado') {
                                                                        echo '<div class="badge"  style="background-color: dark ; padding:auto; border-color: #E97F02">' . $movi->st_os . '</div>';
                                                                    }
                                                                    if ($movi->st_os == 'Aberto') {
                                                                        echo '<div  class="badge info" style="background-color: #8A9B0F; border-color: #8A9B0F">' . $movi->st_os . '</div>';
                                                                    }
                                                                    if ($movi->st_os == 'Resolvendo') {
                                                                        echo '<div class="badge badge-info" >' . $movi->st_os . '</div>';
                                                                    }
                                                                    if ($movi->st_os == 'Parado') {
                                                                        echo '<div class="badge" style="background-color: #CDB380; border-color: #CDB380" >' . $movi->st_os . '</div>';
                                                                    }
                                                                    ?></td>


                                    <td><?php echo $movi->ld_movimentacao; ?></td>


                                    <td> <?php if ($tecnico['0']->id_tecnico == 1) {
                                                echo 'Aguardando Atendimento...';
                                            } else {
                                                echo   $tecnico['0']->nm_funcionario;
                                            } ?></td>




                            <?php }
                        } ?>
                                </tr>


                    </table>
                </div>
                <a class="btn btn-navy align-right" onclick="$('#btn-imprimir').printElement()" id="btn-imprimir"><i class="fa fa-print"></i> Imprimir</a>
                            <a class="btn btn-default align-right" href="<?php echo  base_url() ?>index.php/os"><i class="fa fa-close"></i> Fechar</a>
            </div>
        </div>
    </div>

</div>