<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="background-color:#eeeeee">
        <h1>
            funcionarios
            <small>Editar funcionario</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Dasboard</a></li>
            <li class="active"><?php if ($this->uri->segment(1) != null) { ?><a href="<?php echo base_url() . 'index.php/' . $this->uri->segment(1) ?>" class="tip-bottom" title="<?php echo ucfirst($this->uri->segment(1)); ?>"> <?php echo ucfirst($this->uri->segment(1)); ?></a><?php }; ?>  </li>
        </ol>
    </section>





    <section class="content">
    <?php
        get_msg('salvo');
        ?>
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">

                <div class="text-bold text-blue">  Novo funcionario</div>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!--aqui vem a tabela que vai vir do banco de dados -->

                <div class="row">
                    <!-- left column -->
                    <div class="col-lg-12">

                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="post" id="formUsuarios" action="<?php echo base_url() . 'index.php/funcionario/update'; ?>">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nome</label>

                                    <input type="text" class="form-control     " id="txtnome" name="nome" value="<?php echo $funcionario[0]->nm_funcionario; ?>" placeholder="Nome.">
                                    <?php echo form_error('nome', '<div class="text-danger">', '</div>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Secretario</label>

                                    <input type="text" class="form-control     " id="txtnome" name="secretario" value="<?php echo $funcionario[0]->nm_secretario; ?>" placeholder="Nome.">
                                    <?php echo form_error('secretario', '<div class="text-danger">', '</div>'); ?>
                                </div>
                                <input type="hidden" name="id_funcionario" value="<?php echo $funcionario[0]->id_funcionario; ?>">
                                   
                                

                                <!-- /.box-body -->

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Atualizar</button>
                                </div>
                        </form>
                    </div>

                </div>


                </section>

            


            </div>

            <!-- /.content-wrapper -->




          