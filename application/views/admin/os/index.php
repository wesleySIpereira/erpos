<script>
    setTimeout(function () {
        window.location.reload(1);
    }, 180000); // 3 minutos
</script>    
<div class="row">
    <script>
        function msg() {
            $('.mb-xs').trigger('click');
        }
    </script> 
    <?php
    get_msg('salvo');
    ?>
    <div class="col-md-12">
        <a href="<?php echo base_url(); ?>index.php/os/novo" class="btn btn-success "><i class="fa fa-plus-circle"></i> Adicionar Chamado</a>
        <br><br>
        <div class="panel-group" id="accordion">
            <div class="panel panel-accordion">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <a class="accordion-toggle  collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1One" aria-expanded="false">
                            Busca Avançada
                        </a>
                    </h6>
                </div>
                <div id="collapse1One" class="accordion-body <?php
                if ($filtro) {
                    echo 'in';
                } else {
                    echo '';
                }
                ?> collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <form action="" method="get">



                            <!--aquiv vai mais um o nome p/ busca -->
                            <div class="col-md-3">
                                <label>Secretaria</label>
                                <select class="form-control select2" id="os_id_secretaria" name="id_secretaria" onchange="os_busca_setor($(this).val())" style="width: 100%;">
                                    <option></option>
                                    <?php
                                    if ($this->session->userdata('secretaria')) {
                                        echo $this->session->userdata('secretaria');
                                        foreach ($this->session->userdata('secretaria') as $secretarias) {
                                            ?>
                                            <option value="<?php echo $secretarias->id_secretaria; ?>"><?php echo $secretarias->nm_secretaria; ?></option>

                                        <?php }
                                    }
                                    ?>

                                    } ?>
                                    <?php if ($secretaria) {
                                        foreach ($secretaria as $secretarias) {
                                            ?>
                                            <option value="<?php echo $secretarias->id_secretaria; ?>"><?php echo $secretarias->nm_secretaria; ?></option>
        <?php
    }
}
?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Setor</label>
                                    <select class="form-control select2" id="os_id_setor" name="id_setor" style="width: 100%;">
                                        <option></option>

                                    </select>

                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control select2" id="st_os" name="st_os" style="width: 100%;">
                                        <option></option>
                                        <option value="Aberto">Aberto</option>
                                        <option value="Resolvendo">Resolvendo</option>
                                        <option value="Parado">Parado</option>
                                        <option value="Cancelado">Cancelado</option>
                                        <option class="text-green" value="Fechado">Fechado</option>
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Data Inicial</label>
                                    <input name="data1" id="data1" placeholder="Data Inicial" class="form-control date datepicker hasDatepicker" value="" type="text">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Data Final</label>
                                    <input name="data2" id="data2" placeholder="Data Final" class="form-control date datepicker hasDatepicker" value="" type="text">
                                </div>
                            </div>
                            <!--aquiv vai mais um o nome p/ busca -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Funcionário</label>
                                    <input type="text" class="form-control select2" id="os_busca_nome" name="busca_nome" style="width: 100%;">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Técnico</label>
                                    <select class="form-control select2" id="st_os" name="cid_tecnico" style="width: 100%;">
                                        <option></option>
<?php foreach ($tecnico as $tecnicos) { ?>
    <?php if ($tecnicos->id_tecnico <> '1') { ?>
                                                <option value="<?php echo $tecnicos->id_tecnico; ?>"><?php echo $tecnicos->nm_funcionario; ?></option>
    <?php } ?>
<?php } ?>


                                    </select>

                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><br> </label>
                                    <input type="submit" class="form-control select2 btn <?php
if ($filtro) {
    echo 'btn bg-quartenary';
} else {
    echo 'btn btn-info';
}
?>" id="os_busca_nome" name="buscar" value="<?php if ($filtro) {
    echo 'Desativar Filtro';
} else {
    echo 'Ativar Filtro';
} ?>" style="width: 100%;">
                                </div>
                            </div>



                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>

</div>

<section class="panel">

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-condensed mb-none">


                <th>#OS</th>
                <th>Data</th>
                <th>Hora</th>
                <th>Título</th>
                <th>Secretaria</th>
                <th>Setor</th>
                <th>Funcionário</th>
                <th>Status</th>
                <th>Ramal/Tel.</th>

                <th>Ações</th>
                </tr>
                </thead>
                <tbody style="text-align: center;">
                    <tr>
                            <?php if ($os) {
                                foreach ($os as $oss) { ?>

                                <td><?php echo $oss->id_os; ?></td>
                                <td><?php echo formataVisao($oss->dt_os); ?></td>
                                <td><?php echo $oss->hr_os; ?></td>
                                <td><?php echo $oss->titulo_os; ?></td>
                                <td><?php echo $oss->nm_secretaria; ?></td>
                                <td><?php echo $oss->nm_setor; ?></td>
                                <td><?php echo $oss->nm_funcionario; ?></td>
                                <td class="text-white text-center"><?php
                            if ($oss->st_os == 'Aberto')
                                echo '<div class="badge info" style="background-color: #8A9B0F; border-color: #8A9B0F">' . $oss->st_os . '</div>';
                            if ($oss->st_os == 'Resolvendo') {
                                echo '<div class="badge badge-info"  >' . $oss->st_os . '</div>';
                            }
                            if ($oss->st_os == 'Cancelado') {
                                echo '<div class="badge" style="background-color: #d81b60; border-color: #E97F02" >' . $oss->st_os . '</div>';
                            }
                            if ($oss->st_os == 'Fechado') {
                                echo '<div class="badge " style="background-color: green; border-color: #E97F02"> Finalizado </div>';
                            }
                            if ($oss->st_os == 'Parado') {
                                echo '<div class="badge" style="background-color: #CDB380; border-color: #CDB380" >' . $oss->st_os . '</div>';
                            }
                                    ?></td>
                                <td><?php echo $oss->n_ramal; ?></td>

                                <td> 
                                    <div class="btn-group-horiontal">
                                        <a href="<?php echo base_url(); ?>index.php/os/detalhe/<?php echo encript($oss->id_os); ?>" title="visualizar" class="btn btn-xs btn-flat btn-info "><i class="fa fa-eye"></i></a>
        <?php if ($oss->st_os <> 'Fechado') { ?>
                                            <a href="<?php echo base_url(); ?>index.php/os/answer/<?php echo encript($oss->id_os); ?>" class="btn btn-flatbtn btn-info btn-xs  " title="Atender chamado"><i class="fa fa-phone"></i></a>
                            <?php } else { ?>
                                            <a title="Fechado" class="btn btn-xs btn-flat btn-default"><i class="fa fa-lock"></i></a>
                            <?php } ?>
                                        <a href="<?php echo base_url(); ?>index.php/os/imprime-chamado/<?php echo encript($oss->id_os); ?>" title="Imprimir" class="btn btn-flat btn-xs btn-info"><i class="fa fa-print"></i></a>
                                    </div>
                                </td>
                            </tr>
    <?php }
} else { ?>    

                    <td colspan="10"><center>Nenhum Chamado Cadastrado</center> </td>
                    </tr>
<?php } ?>
                </tbody>
            </table>
            <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">  
                <ul class="pagination">
<?php
if (isset($pag) && !empty($pag)) {
    foreach ($pag as $key => $value) {
        echo " {$value} ";
    }
};
?>
            </div>
        </div>
</section>





<!-- modal visualização -->

<!-- Modal Info -->

<?php
if ($os) {
    foreach ($os as $oss) {
        ?>
        <div class="modal fade modal-info " id="ver_<?php echo $oss->id_os; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>

                    </div>
                    <div class="modal-body">

                        <div class="text-blue"> <b>Nº Chamado:</b> <?php echo $oss->id_os; ?><br>

                            <b>Status: </b><?php echo $oss->st_os; ?> </b>
                            <br>
                            <b>Data chamado: </b><?php echo formataVisao($oss->dt_os); ?> <b>Horário: </b><?php echo $oss->hr_os; ?>
                            <br>
                            <b>Funcionário: </b><?php echo $oss->nm_funcionario; ?>
                            <br>
                            <b>Nº Resp: </b><?php echo $oss->n_resp; ?>
                            <br>
                            <b>Nº Ramal/Telefone: </b><?php echo $oss->n_ramal; ?>
                            <br>
                            <b>Secretaria: </b><?php echo $oss->nm_secretaria; ?>
                            <br>
                            <b>Setor: </b><?php echo $oss->nm_setor; ?>
                            <br>
                            <b>Local: </b><?php echo $oss->nm_local; ?>
                            <br>
                            <b>Problema: </b><textarea class="col-lg-12 form-control text-blue " disabled="true" name="ob_os" id="ob_os" cols="30" rows="5"><?php echo $oss->df_os; ?></textarea>
                            <br>
                            <b>Observação: </b><textarea class="col-lg-12 form-control text-blue" disabled="true" name="ob_os" id="ob_os" cols="30" rows="5"><?php echo $oss->ob_os; ?></textarea>
                            <br>
                            </br>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default align-right" data-dismiss="modal"><i class="fa fa-close"></i> Fechar</button>

                    </div>
                </div>
            </div>
        </div>

        <?php
    }
}
?>  

<!-- modal realmente deletar -->
<?php
if ($os) {
    foreach ($os as $oss) {
        ?>
        <form action="<?php echo base_url() ?>index.php/cargo/delete/<?php echo encript($oss->id_os); ?>" method="get" >
            <div class="modal fade" id="modal-excluir_<?php echo $oss->id_os; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>

                        </div>
                        <div class="modal-body">

                            <div class="text-bold ">
                                <h4><center> Deseja realmente apagar esse registro?</center></h4>
                                <br> 
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                            <input type="submit" class="btn btn-danger" value="Excluir">

                        </div>
                    </div>
                </div>
            </div>
        </form>    
        <?php
    }
}
?>  
