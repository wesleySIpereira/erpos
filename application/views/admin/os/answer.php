<div class="row">
    <script>
        function msg() {
            $('.mb-xs').trigger('click');
        }
    </script>
    <?php
    get_msg('salvo');
    ?>
    <div class="col-lg-12">
        <section class="panel form-wizard" id="w1">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title"><?php echo $titulo; ?></h2>
            </header>
            <div class="panel-body panel-body-nopadding">
                <div class="wizard-tabs">
                    <ul class="wizard-steps">
                        <li class="active">
                            <a href="#w1-account" data-toggle="tab" class="text-center" aria-expanded="false">
                                <span class="badge hidden-xs">..</span>
                                Dados do Chamado
                            </a>
                        </li>
                        <li class="">
                            <a href="#w1-profile" data-toggle="tab" class="text-center" aria-expanded="false">
                                <span class="badge hidden-xs">..</span>
                                Atender
                            </a>
                        </li>

                    </ul>
                </div>

                <div class="tab-content">
                    <div id="w1-account" class="tab-pane active ">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td style="text-align: right; width: 11%"><strong> <b>Nº Chamado:</b></strong></td>
                                    <td colspan="8"><?php echo $os[0]->id_os; ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right"><strong><b>Data chamado: </b></strong></td>
                                    <td><?php echo formataVisao($os[0]->dt_os); ?></td>
                                    <td style="text-align: right;"><b>Horário: </b></td>
                                    <td colspan="5"><?php echo $os[0]->hr_os; ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right"><strong><b>Funcionário: </b></strong></td>
                                    <td colspan="8"><?php echo $os[0]->nm_funcionario; ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right"><strong><b>Secretaria: </b></strong></td>
                                    <td colspan=""><?php echo $os[0]->nm_secretaria; ?></td>
                                    <td style="text-align: right"><strong><b>Setor: </b></strong></td>
                                    <td colspan=""><?php echo $os[0]->nm_setor; ?></td>
                                    <td style="text-align: right"><strong><b>Local: </b></strong></td>
                                    <td colspan=""><?php echo $os[0]->nm_local; ?></td>
                                    <td style="text-align: right"><strong><b>Ramal: </b></strong></td>
                                    <td colspan=""><?php echo $os[0]->n_ramal; ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right"><strong><b>Título: </b></strong></td>
                                    <td colspan="8"><?php echo $os[0]->titulo_os; ?></td>
                                </tr>
                                <?php //pesquisa para saber quem foi o filha da puta que atendeu essa porra p/ passar informação p/ cliente pau no cu
                                $id_tecnico = $os[0]->id_tecnico;
                                $like = array(
                                    'id_tecnico' => $id_tecnico
                                );
                                $tecnico = $this->Mos->listaOsTecnicoLike($p = 0, $por_pagina = null, 'v_tecnico', '*', $like, $porData = null, $order = null);   ?>
                                <tr>
                                    <td style="text-align: right"><strong> <b>Problema: </b></strong></td>
                                    <td colspan="8"><?php echo $os[0]->df_os; ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right"><strong> <b>Observação: </b></strong></td>
                                    <td colspan="8"><?php echo $os[0]->ob_os; ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right; width: 13%"><strong>Status:</strong></td>
                                    <td colspan="7"><?php if ($os[0]->st_os == 'Fechado') {
                                                        echo '<div class="badge " style="background-color: green; border-color: #E97F02"> Finalizado </div>';
                                                    }
                                                    if ($os[0]->st_os == 'Cancelado') {
                                                        echo '<div class="badge"  style="background-color: dark ; padding:auto; border-color: #E97F02">' . $os[0]->st_os . '</div>';
                                                    }
                                                    if ($os[0]->st_os == 'Aberto') {
                                                        echo '<div  class="badge info" style="background-color: #8A9B0F; border-color: #8A9B0F">' . $os[0]->st_os . '</div>';
                                                    }
                                                    if ($os[0]->st_os == 'Resolvendo') {
                                                        echo '<div class="badge badge-info" >' . $os[0]->st_os . '</div>';
                                                    }
                                                    if ($os[0]->st_os == 'Parado') {
                                                        echo '<div class="badge" style="background-color: #CDB380; border-color: #CDB380" >' . $os[0]->st_os . '</div>';
                                                    }
                                                    ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;"><strong>Data Atendimento:</strong></td>
                                    <td><?php if (isset($os[0]->dt_atendimento)) {
                                            echo formataVisao($os[0]->dt_atendimento);
                                        } else {
                                            echo 'Aguardando...';
                                        } ?> </td>

                                    <td style="text-align: right"><strong>Horário:</strong></td>
                                    <td colspan="1"><?php if (isset($os[0]->hr_atendimento)) {
                                                        echo $os[0]->hr_atendimento;
                                                    } else {
                                                        echo 'Aguardando...';
                                                    } ?></td>
                                    <td colspan="" style="text-align: right;"><strong>Técnico Responsável:</strong></td>
                                    <td colspan="3"> <?php if ($tecnico['0']->id_tecnico == 1) {
                                                            echo 'Aguardando Atendimento...';
                                                        } else {
                                                            echo   $tecnico['0']->nm_funcionario;
                                                        } ?></td>
                                </tr>

                                <tr>
                                    <td style="text-align: right;"><strong>Solução:</strong></td>
                                    <td colspan="8"><?php echo $os[0]->ld_tecnico; ?></td>
                                </tr>

                            </tbody>
                        </table>

                    </div>
                    <div id="w1-profile" class="tab-pane">
                        <form action="<?php echo base_url(); ?>index.php/os/save" class="form-group" method="post" id="formOs">

                            <div class="col-lg-12">
                                <div class="col-lg-4"> </br><label for="cliente">Status<span class="required">*</span></label>
                                    <select id="st_os" name="st_os" class="form-control     ">
                                        <option value="<?php echo $os[0]->st_os; ?>"><?php echo $os[0]->st_os; ?></option>
                                        <option value="Resolvendo">Resolvendo</option>
                                        <option value="Parado">Parado</option>
                                        <option value="Cancelado">Cancelado</option>
                                        <option value="Fechado">Finalizar</option>
                                    </select>

                                </div>
                                <div class="col-lg-2"></br> <label for="cliente">Data Atendimento<span class="required">*</span></label>
                                    <input id="dt_atendimento" class="span12 form-control     " type="text" name="dt_atendimento" disabled value="<?php if ($os[0]->dt_atendimento) {
                                                                                                                                                        echo formataVisao($os[0]->dt_atendimento);
                                                                                                                                                    } else {
                                                                                                                                                        echo date('d/m/Y');
                                                                                                                                                    } ?> " />
                                    <input id="dt_atendimento" class="span12 form-control     " type="hidden" name="dt_atendimento" value="<?php if ($os[0]->dt_atendimento) {
                                                                                                                                                echo formataVisao($os[0]->dt_atendimento);
                                                                                                                                            } else {
                                                                                                                                                echo date('d/m/Y');
                                                                                                                                            } ?> " />
                                </div>
                                <div class="col-lg-2"></br> <label for="cliente">Hora Atendimento<span class="required">*</span></label>
                                    <input id="hr_atendimento" class="span12 form-control     " type="text" name="hr_atendimento" disabled value="<?php if ($os[0]->hr_atendimento) {
                                                                                                                                                        echo $os[0]->hr_atendimento;
                                                                                                                                                    } else {
                                                                                                                                                        echo date("H:i:s");
                                                                                                                                                    } ?>" />
                                    <input id="hr_atendimento" class="span12 form-control     " type="hidden" name="hr_atendimento" disabled value="<?php if ($os[0]->hr_atendimento) {
                                                                                                                                                        echo $os[0]->hr_atendimento;
                                                                                                                                                    } else {
                                                                                                                                                        echo date("H:i:s");
                                                                                                                                                    } ?>" />
                                </div>

                                <?php if ($tecnico[0]->id_tecnico == '1') { ?>
                                    <div class="col-lg-4"></br> <label for="tecnico">Técnico / Responsável<span class="required ">*</span></label>
                                        <select class="form-control      select2" name="id_tecnico" style="width: 100%;">
                                            <option selected="selected" value="null">Em Aberto</option>

                                        </select>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-lg-4"><br> <label for="tecnico">Técnico / Responsável<span class="required ">*</span></label>
                                        <input type="text" disabled value="<?php echo $tecnico[0]->nm_funcionario; ?>" class=" form-control      select2" name="id_tecnico" style="width: 100%;">


                                    </div>
                                <?php } ?>
                                <!--Tecnico responsavel 
  <div class="col-lg-5"></br> <label for="cliente">Técnico Responsavél<span class="required">*</span></label>
    <input id="nda" class="span12 form-control     " type="text" name="nada" disabled value="<?php echo $tecnico[0]->nm_funcionario; ?> " />
    <input id="id_tecnico" class="span12 form-control     " type="hidden" name="id_tecnico" disabled value="<?php echo $os[0]->id_tecnico; ?> " />
  </div>
   fim -->
                                <div class="col-lg-12"></br> <label for="cliente">Solução chamado<span class="required">*</span></label>
                                    <textarea class="col-lg-12 form-control      " name="ld_tecnico" id="ld_tecnico" cols="30" rows="5"><?php echo $os[0]->ld_tecnico; ?></textarea>
                                </div>
                                <a data-toggle="modal" data-target="#modal-logar_<?php echo $os[0]->id_os; ?>" style="margin: 10px;" class="btn btn-success "><i class="fa fa-check"></i> Atualizar</a>
                        </form>
                       
                                   

                                
                    </div>
                </div>

            </div>

    </div>

    </section>
</div>
</div>




<!--MODAL LOGIN PARA ALTERAR OS -->


<div class="modal fade" id="modal-logar_<?php echo $os[0]->id_os; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS </h4>

            </div>
            <div class="modal-body">

                <div class="text-bold ">
                    Por favor digite sua senha de Usuário.

                    <input id="user" class="span12 form-control     " type="text" name="user" <?php if ($tecnico[0]->id_tecnico == '1') {
                                                                                                    echo ' value="">';
                                                                                                } else {
                                                                                                    echo 'value="' . $os[0]->usuario . '" disabled >';
                                                                                                } ?> </br> </br> <label for="cliente">Senha<span class="required">*</span></label>
                    <input id="senha" class="span12 form-control     " type="password" name="senha" />


                    <br>

                    <div id="erro">
                    </div>

                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <script language="javascript">
                        var id1 = "<?php echo $os[0]->id_os; ?>";
                    </script>
                    <input type="submit" class="btn btn-danger" onclick="logar()" value="logar">

                </div>
            </div>
        </div>
    </div>
</div>







<!-- FIM MODAL -->