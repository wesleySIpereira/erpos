<div class="row">
    <?php
    get_msg('salvo');
    ?>
    <div class="col-md-12">
        <a href="<?php echo base_url(); ?>index.php/funcionario/novo" class="btn btn-success "><i class="fa fa-plus-circle"></i> Adicionar Funcionário</a>
        <br><br>
        <div class="panel-group" id="accordion">
            <div class="panel panel-accordion">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <a class="accordion-toggle  collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1One" aria-expanded="false">
                            Busca Avançada
                        </a>
                    </h6>
                </div>
                <div id="collapse1One" class="accordion-body <?php if ($filtro) {
                                                                    echo 'in';
                                                                } else {
                                                                    echo '';
                                                                } ?> collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <form action="" method="get">



                            <!--aquiv vai mais um o nome p/ busca -->
                            <div class="col-md-4">
                                <label>Secretaria</label>
                                <select class="form-control select2" id="os_id_secretaria" name="id_secretaria" onchange="os_busca_setor($(this).val())" style="width: 100%;">
                                    <option></option>

                                    <?php if ($secretaria) {

                                        foreach ($secretaria as $secretarias) { ?>
                                            <option value="<?php echo $secretarias->id_secretaria; ?>">
                                                <?php echo $secretarias->nm_secretaria; ?></option>
                                    <?php
                                        }
                                    } ?>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Setor</label>
                                    <select class="form-control select2" id="os_id_setor" name="id_setor" style="width: 100%;">
                                        <option></option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Cargo</label>
                                    <select class="form-control select2" id="st_os" name="cid_cargo" style="width: 100%;">
                                        <option></option>
                                        <?php if ($cargo1) {
                                            foreach ($cargo1 as $cargos) { ?>
                                                <option value="<?php echo $cargos->id_cargo; ?>"><?php echo $cargos->nm_cargo; ?></option>
                                        <?php
                                            }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Funcionário</label>
                                    <input type="text" class="form-control select2" id="os_busca_nome" name="busca_nome" style="width: 100%;">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><br> </label>
                                    <input type="submit" class="form-control select2 btn <?php if ($filtro) {
                                                                                                echo 'btn bg-quartenary';
                                                                                            } else {
                                                                                                echo 'btn btn-info';
                                                                                            } ?>" id="os_busca_nome" name="buscar" value="<?php if ($filtro) {
                                                                                                                                                echo 'Desativar Filtro';
                                                                                                                                            } else {
                                                                                                                                                echo 'Ativar Filtro';
                                                                                                                                            } ?>" style="width: 100%;">
                                </div>
                            </div>



                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>

</div>

<section class="panel">

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-condensed mb-none">


                <th>#id</th>
                <th>Funcionário</th>
                <th>Resp</th>
                <th>Cargo</th>
                <th>Secretaria</th>
                <th>Setor</th>
                <th>Ramal/Telefone</th>

                <th>Ações</th>
                </tr>
                </thead>
                <tbody style="text-align: center;">
                    <tr>
                        <?php if ($funcionario) {
                            foreach ($funcionario as $funcionarios) { 
                             /* NÃO MOSTRA O PRIMEIRO USUARIO POIS É O ADMINISTRADOR DO SISTEMA */
                                if($funcionarios->id_funcionario <>'1'){?>

                                <td><?php echo $funcionarios->id_funcionario; ?></td>
                                <td><?php echo $funcionarios->nm_funcionario; ?></td>
                                <td><?php echo $funcionarios->n_resp; ?></td>
                                <td><?php echo $funcionarios->nm_cargo; ?></td>
                                <td><?php echo $funcionarios->nm_secretaria; ?></td>
                                <td><?php echo $funcionarios->nm_setor; ?></td>
                                <td><?php echo $funcionarios->n_ramal; ?></td>
                                <td>
                                    <div class="btn-group-horiontal">
                                        <a data-toggle="modal" data-target="#ver_<?php echo $funcionarios->id_funcionario; ?>" title="visualizar" class="btn btn-xs btn-flat btn-default"><i class="fa fa-eye"></i></a>
                                        <a href="<?php echo base_url(); ?>index.php/funcionario/edit/<?php echo encript($funcionarios->id_funcionario); ?>" title="editar" class="btn btn-xs btn-flat btn-info"><i class="fa fa-edit"></i></a>
                                        <a data-toggle="modal" data-target="#modal-excluir_<?php echo $funcionarios->id_funcionario; ?>" title="excluir" class="btn btn-xs btn-flat btn-danger"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                </td>
                    </tr>
                <?php }}
                        } else { ?>

                <td colspan="4">
                    <center>Nenhuma secretaria cadastrada</center>
                </td>
                </tr>
            <?php } ?>
                </tbody>
            </table>
            <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                <ul class="pagination">
                    <?php if (isset($pag) && !empty($pag)) {
                        foreach ($pag as $key => $value) {
                            echo " {$value} ";
                        }
                    }; ?>
            </div>
        </div>
</section>





<!-- modal visualização -->

<!-- Modal Info -->

<?php
if ($funcionario) {
    foreach ($funcionario as $funcionarios) {
?>
        <div class="modal fade modal-info " id="ver_<?php echo $funcionarios->id_funcionario; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS </h4>

                    </div>
                    <div class="modal-body">

                        <div class="text-info"> <b>Id:</b> <?php echo $funcionarios->id_funcionario; ?><br>
                            <b>Funcionário: </b><?php echo $funcionarios->nm_funcionario; ?>
                            <br>
                            <b>Nº Resp: </b><?php echo $funcionarios->n_resp; ?>
                            <br>
                            <b>E-mail: </b><?php echo $funcionarios->mail_funcionario; ?>
                            <br>
                            <b>Cargo: </b><?php echo $funcionarios->nm_cargo; ?>
                            <br>
                            <b>Nome de Usuário do Sistema: </b><?php echo $funcionarios->nm_usuario; ?>
                            <br>
                            <b>Secretaria: </b><?php echo $funcionarios->nm_secretaria; ?>
                            <br>
                            <b>Setor: </b><?php echo $funcionarios->nm_setor; ?>
                            <br>
                            <b>Local: </b><?php echo $funcionarios->nm_local; ?>
                            <br>
                            <b>Obs local de trabalho: </b><?php echo $funcionarios->obs_local; ?>
                            <br>
                            <b>Responsável do Setor: </b><?php echo $funcionarios->nm_responsavel; ?>
                            <br>
                            <b>Nº Ramal/Telefone: </b><?php echo $funcionarios->n_ramal; ?>
                            <br>
                            <b>Status: </b><?php if ($funcionarios->ativo) {
                                                echo 'ativo';
                                            } else {
                                                echo 'inativo';
                                            } ?>
                        </div>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default align-right" data-dismiss="modal"><i class="fa fa-close"></i> Fechar</button>

                    </div>
                </div>
            </div>
        </div>

<?php
    }
}
?>

<!-- modal realmente deletar -->
<?php
if ($funcionario) {
    foreach ($funcionario as $funcionarios) {
?>
        <form action="<?php echo base_url() ?>index.php/funcionario/delete/<?php echo encript($funcionarios->id_funcionario); ?>" method="get">
            <div class="modal fade" id="modal-excluir_<?php echo $funcionarios->id_funcionario; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS </h4>

                        </div>
                        <div class="modal-body">

                            <div class="text-bold ">
                                <h4>
                                    <center> Deseja realmente apagar esse registro?</center>
                                </h4>
                                <br>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                            <input type="submit" class="btn btn-danger" value="Excluir">

                        </div>
                    </div>
                </div>
            </div>
        </form>
<?php
    }
}
?>