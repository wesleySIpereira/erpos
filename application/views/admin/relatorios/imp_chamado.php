<?php


class PDF extends FPDF
{
// Page header
function Header()
{
    // Logo
    //$Image=base_url('assets/img/logo.png');
    //$this->Image($Image,10,6,30);
    // Arial bold 15
    $this->SetFont('Arial','',10);
    // Move to the right
   // $this->Cell(0);
    // Title
    $this->Cell(190,5,'ERPOS |Sistema de chamados','TLR',1,'C');
    
    
    $this->SetFont('Arial','',8);
    $this->Cell(190,5,'CPD/TI Ramais: 500,476 e 213','LR',1,'C');
    $this->SetFont('Arial','',9);
    $this->Cell(190,5,utf8_decode('ERPOS | CPD Prefeitura Municipal de Patrocínio'),'LRB','','L');

   
    
    
    $this->Ln(10);
}

function body($os,$tecnico){
$this->SetFont('Arial','B',9); 
 $this->Cell(30,5,utf8_decode('Nº Chamado: ').$os[0]->id_os.'','LRTB','','C');   
 $this->Cell(110);
 $this->Cell(50,5,utf8_decode('Gerado: ').date('d/m/Y').' as '.date('H:i:s').'','LRBT','','C');  
 $this->Ln(10);
 $this->SetFont('Arial','',9); 
 $this->Cell(190,5,utf8_decode('Data: '.formataVisao($os[0]->dt_os).'                                                   Horário: '.$os[0]->hr_os.''),'TLR','','');
 $this->Ln(5);
 $this->Cell(190,5,utf8_decode('Funcionário: '.$os[0]->nm_funcionario.''),'LR','','');
 $this->Ln(5);
 $this->Cell(190,5,utf8_decode('Secretaria: '.$os[0]->nm_secretaria.''),'LR','','');
 
  $this->Ln(5);
 $this->Cell(190,5,utf8_decode('Setor: '.$os[0]->nm_setor.''),'LR','','');   
 $this->Ln(5);
 $this->Cell(190,5,utf8_decode('Local: '.$os[0]->nm_local.''),'LR','','');  
 $this->Ln(5);
 $this->Cell(190,5,utf8_decode('Ramal/Tel: '.$os[0]->n_ramal.''),'LRB','','');
 $this->Ln(10);
 $this->SetFont('Arial','',9);

 $this->Cell(190,5,utf8_decode('Título:=> '.$os[0]->titulo_os.''),'LRTB','','');
 $this->ln(10);
 $this->MultiCell(190,5,utf8_decode('Problema:=> '.$os[0]->df_os.''),'LRTB','','');
 $this->ln(6);

 $this->SetFont('Arial','',9);
 $this->MultiCell(190,5,utf8_decode('Obs:=> '.$os[0]->ob_os.''),'LRBT','','');
 $this->SetFont('Arial','',9);
 $this->Ln(5);
 if(isset($os[0]->dt_atendimento)){
 $this->Cell(190,5,utf8_decode('Data Atendimento: '.formataVisao($os[0]->dt_atendimento).'      Horário: '.$os[0]->hr_os.'     Status: '.$os[0]->st_os.'              Técnico: '.$tecnico[0]->nm_funcionario.''),'LTBR','','','');   
 }else{
    $this->Cell(190,5,utf8_decode('Data Atendimento:                             Horário:                       Status: '.$os[0]->st_os.'              Técnico:                  '),'LRTB','','');      
 }
 $this->Ln(7);
 $this->Cell(30,1,utf8_decode('Solução:'),0,0,''); 
 $this->Ln(3);
 if($os[0]->ld_tecnico==''){
 $this->MultiCell(190,40,utf8_decode($os[0]->ld_tecnico),'LTBR','T','',true);
 }else{
    $this->MultiCell(190,5,utf8_decode($os[0]->ld_tecnico),'LTBR','T','',true);   
 }

 $this->Ln(30);
 $this->Cell(30,1,utf8_decode('Funcionário:________________________ '),'','','');
 $this->Cell(80);
$this->Cell(30,1,utf8_decode('Responsável Técnico:________________________'),'','','');
$this->Ln(5);

 
}

}
// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Página '.$this->PageNo().'/{nb}',0,0,'C');
}
// Simple table





// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();

$pdf->AddPage();
$pdf->Footer();

//$pdf->SetFont('Arial','',10);
//busca por tecnico 



$pdf->body($os,$tecnico);
$pdf->Output();