<div class="row">
    <div class="col-md-12">
        <?php
        get_msg('salvo');
        ?>
      
         <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>

                    <h2 class="panel-title"><?php echo $titulo; ?></h2>
                    <p class="panel-subtitle">

                    </p>
                </header>
             <div class="panel-body">
                   <form role="form" method="post" id="form_produto"    action="<?php echo base_url() . 'index.php/produto/save'; ?>">
                 <div  class="form-group">
                                    <label for="exampleInputEmail1">Nome</label>

                                    <input type="text" class="form-control     " id="nome"  name="nm_produto"    value="<?php echo set_value('nm_produto'); ?>" >
                                    <span id="nm_produto-erro" class="text-danger  " style="display:none">Este Funcionário já esta cadatrado.</span>
                                   
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Descrição</label>

                                    <input type="text" required class="form-control     " id="desc_produto" onchange="verificaEmailExiste()" name="desc_produto" value="<?php echo set_value('desc_produto'); ?>" >
                                    <?php echo form_error('mail_produto', '<div class="text-danger">', '</div>'); ?>
                                    
                                </div>
                                
                
                   <div class="form-group">
                                    <label>Grupo</label>
                                    <select class="form-control select2" id="os_id_categoria" name="id_categoria" onchange="os_busca_categoria($(this).val())" style="width: 100%;">
                                    <option ></option>
                                    <?php if($this->session->userdata('categoria')){
                                        echo $this->session->userdata('categoria');
                                        foreach ($this->session->userdata('categoria') as $categorias) {?>
                                         <option value="<?php echo $categorias->id_categoria; ?>" ><?php echo $categorias->nm_categoria; ?></option>
                                           
                                      <?php  }} ?>

                                    } ?>
                                    <?php if ($categoria) {
                                        foreach ($categoria as $categorias) { ?>
                                    <option value="<?php echo $categorias->id_categoria; ?>" ><?php echo $categorias->nm_categoria; ?></option>
                                    <?php 
                                }
                            } ?>
                                     </select>
                                        
                              </div>   
                              
                 
                  
                  <div class="form-group">
                                <label>Sub Grupo</label>
                                <select class="form-control select2" id="os_id_setor"  name="id_subgrupo"  style="width: 100%;">
                                <option></option>
                               
                                </select>
                                        
                            </div> 
                




                                <div class="form-group">
                                    <label for="exampleInputEmail1">Código</label>

                                    <input type="text" class="form-control     " id="n_resp" name="cod_sysdardani" onchange="verificaNumeroResp()" value="<?php echo set_value('cod_sysdardani'); ?>" >
                                    <?php echo form_error('n_resp', '<div class="text-danger">', '</div>'); ?>
                                    
                                </div>
                               
                            <div class="form-group">
                                <label>Unidade</label>
                                <select class="form-control      select2" id="unidade" name="unidade"  style="width: 100%;">
                                <option>UN</option>
                                <option>Kl</option>
                                </select>
                                        
                            </div>   
                          
                               
                 
                            </div>   
              <footer class="panel-footer">
        <button onsubmit="verificaUnicidade()" type="submit" class="btn btn-success"><i class="fa fa-check"></i> Adicionar</button>

    </footer>
                            </form>
      
                    
         </section>   
             </div> 
     
    </div>

