
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Produto
        <small>Gerenciar produtos</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dasboard</a></li>
        <li class="active">produto</li>
      </ol>
    </section>
     <hr>
    <!-- Main content -->
    <section class="content container-fluid">
    <?php
        get_msg('salvo');
        ?>
          <div class="row">
        <div class="col-xs-12">
          <div class="">
            <div class="">
            <div class="align-right"><a href="<?php echo base_url();?>index.php/produto/novo" class="btn btn-primary  "><i class="fa fa-plus-circle"></i> Adicionar produto</a>
            </div>
            </div>
            <!--AQUI VAI ADIÇÃO FILTROS DE BUSCA -->  
          
            <div class="accordion-group widget-box">
                    <div class="accordion-heading">
                        <div class="widget-title "> <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse"> <span class="icon text-right"><i class="icon-list"></i></span>
                                <h5>Busca Avançada</h5>
                            </a> </div>
                    </div>
                    <div class="accordion-body <?php if($filtro){echo 'in';}else{echo '';} ?>  collapse" id="collapseGOne" style="height: auto;">
                        <div class="widget-content">
                        <div class="box-body">

            <div class="row">
                <form action="" method="get">
                   <div class="col-md-3">
                   <div class="form-group">
                                    <label>Grupo</label>
                                    <select class="form-control select2" id="os_id_categoria" name="id_categoria" onchange="os_busca_categoria($(this).val())" style="width: 100%;">
                                    <option ></option>
                                    <?php if($this->session->userdata('categoria')){
                                        echo $this->session->userdata('categoria');
                                        foreach ($this->session->userdata('categoria') as $categorias) {?>
                                         <option value="<?php echo $categorias->id_categoria; ?>" ><?php echo $categorias->nm_categoria; ?></option>
                                           
                                      <?php  }} ?>

                                    } ?>
                                    <?php if ($categoria) {
                                        foreach ($categoria as $categorias) { ?>
                                    <option value="<?php echo $categorias->id_categoria; ?>" ><?php echo $categorias->nm_categoria; ?></option>
                                    <?php 
                                }
                            } ?>
                                     </select>
                                        
                              </div>   
                              
                  </div>  
                  <div class="col-md-3">
                  <div class="form-group">
                                <label>Sub Grupo</label>
                                <select class="form-control select2" id="os_id_setor"  name="id_subgrupo"  style="width: 100%;">
                                <option></option>
                               
                                </select>
                                        
                            </div> 
                </div>
               
                
                <!--aquiv vai mais um o nome p/ busca -->
                <div class="col-md-3">
                  <div class="form-group">
                                <label>produto</label>
                                <input type="text" class="form-control select2" id="os_busca_nome"  name="nm_produto"  style="width: 100%;">       
                            </div> 
                </div>
               
                <div class="col-md-3">
                  <div class="form-group">
                                <label><br> </label>
                                <input type="submit" class="form-control select2 btn  <?php if($filtro){echo 'btn bg-purple';}else{echo 'btn btn-primary';} ?>" id="os_busca_nome"   name="buscar" value="<?php if($filtro){echo 'Desativar Filtro';}else{echo 'Ativar Filtro';} ?>" style="width: 100%;"> 
                            </div> 
                </div>
                
                
                
                </form>
                   </div>
                        </div>
                        </div>
                    </div>
            </div>
            <!--FINAL FILTROS DE BUSCA -->  
            <!-- /.box-header -->
            <div class="widget-box">
                        <div class="widget-title"> <span class="icon"> <i class="icon-user-md"></i> </span>
                            <h5>produto</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered ">
                                
                                <thead>
                                    <tr  style="backgroud-color: #2D335B ;">
                  <th>#id</th> 
                  <th>produto</th>
                  <th>descrição</th>
                  <th>grupo</th>
                  <th>subgrupo</th>
                  <th>codigo</th>
                  <th>unidade</th>
                 
                  <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                <?php if($produto){ foreach ($produto as $produtos) { ?>
                 
                   
                  <td><?php echo $produtos->id_produto;?></td>
                  <td><?php echo $produtos->nm_produto;?></td>
                  <td><?php echo $produtos->desc_produto;?></td>
                  <td><?php echo $produtos->nm_categoria;?></td>
                  <td><?php echo $produtos->nm_subcategoria;?></td>
                  <td><?php echo $produtos->cod_sysdardani;?></td>
                  <td><?php echo $produtos->unidade;?></td>
                  <td> <div class="btn-group-horiontal">
                      <a  data-toggle="modal" data-target="#ver_<?php echo $produtos->id_produto; ?>" title="visualizar" class="btn btn-outline-info btn-info"><i class="fa fa-eye"></i></a>
                      <a href="<?php echo base_url(); ?>index.php/produto/edit/<?php echo encript($produtos->id_produto); ?>"  title="editar" class="btn btn-flat btn-info"><i class="fa fa-edit"></i></a>
                      <a data-toggle="modal" data-target="#modal-excluir_<?php echo $produtos->id_produto; ?>" title="excluir"  class="btn btn-flat btn-info"><i class="fa fa-trash"></i></a>
                    </div></td>
                </tr>
                <?php  } }else{?>    
                
                <td colspan="8"><center>Nenhum produto cadastrado</center> </td>
                </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                  <td colspan="9" style="align:right">    
                  <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">  
                  <ul class="pagination">
                  <?php if(isset($pag) && (!empty($pag))){foreach ($pag as $key => $value) {
                      echo " {$value} ";
                  }}; ?>
                  </ul>
                  </div>
                  </td>
                    </tr>
                   
                </tfoot>
              </table>
                <!-- /.box-body -->
               

                      
            </div>
            <!-- /.box-body -->
          
          <!-- /.box -->

        
        
        <!-- /.col -->
      
      <!-- /.row -->


    </section>
    <!-- /.content -->
  </div>
                
  <!-- /.content-wrapper -->








<!--modal visualizar detalhes -->


<!-- Button trigger modal -->


<!-- Modal-->
<?php
if ($produto) {
    foreach ($produto as $produtos) {
        ?>
        <div class="modal fade" id="ver_<?php echo $produtos->id_produto; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>
                        
                    </div>
                    <div class="modal-body">
                        <div class="text-info"> <b>Id:</b> <?php echo $produtos->id_produto; ?><br>
                            <b>Produto: </b><?php echo $produtos->nm_produto; ?>
                            <br> 
                            <b>Descrição: </b><?php echo $produtos->desc_produto; ?>
                            <br> 
                            <b>Grupo: </b><?php echo $produtos->nm_categoria; ?>
                            <br> 
                            <b>Sub Grupo: </b><?php echo $produtos->nm_subcategoria; ?>
                            <br> 
                            <b>Código: </b><?php echo $produtos->cod_sysdardani; ?>
                            <br> 
                            <b>Únidade: </b><?php echo $produtos->unidade; ?>
                            <br> 
                           
                            
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default align-right" data-dismiss="modal"><i class="fa fa-close"></i> Fechar</button>

                    </div>
                </div>
            </div>
        </div>

        <?php
    }
}
?>  



<?php
if ($produto) {
    foreach ($produto as $produtos) {
        ?>
        <form action="<?php echo base_url() ?>index.php/produto/delete/<?php echo encript($produtos->id_produto);?>/<?php // echo encript($produtos->id_local);?>" method="get" >
        <div class="modal fade" id="modal-excluir_<?php echo $produtos->id_produto; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>
                        
                    </div>
                    <div class="modal-body">
                   
                        <div class="text-bold ">
                        <h4><center> Deseja realmente apagar esse registro?</center></h4>
         <br> 
                        </div>


                    </div>
                    <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <input type="submit" class="btn btn-danger" value="Excluir">

                    </div>
                </div>
            </div>
        </div>
        </form>    
        <?php
    }
}
?>  