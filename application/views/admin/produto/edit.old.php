<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Produto
            <small>  Editar Produto</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Dasboard</a></li>
            <li class="active"><?php if ($this->uri->segment(1) != null) { ?><a href="<?php echo base_url() . 'index.php/' . $this->uri->segment(1) ?>" class="tip-bottom" title="<?php echo ucfirst($this->uri->segment(1)); ?>"> <?php echo ucfirst($this->uri->segment(1)); ?></a><?php }; ?>  </li>
        </ol>
    </section>





    <section class="content">
    <?php
        get_msg('salvo');
        ?>
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">

                <div class="text-bold ">  Editar produto</div>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!--aqui vem a tabela que vai vir do banco de dados -->

                <div class="row">
                    <!-- left column -->
                    <div class="col-lg-12">

                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="post" id="form_produto" action="<?php echo base_url() . 'index.php/produto/update/'.$produto[0]->id_produto;?>">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nome</label>
                                     <input type="hidden" name="id_produto" value="<?php echo $produto[0]->id_produto;?>" > 
                                    <input type="text" class="form-control     " id="nm_produto" name="nm_produto" value="<?php echo $produto[0]->nm_produto; ?>" >
                                    <?php echo form_error('nome', '<div class="text-danger">', '</div>'); ?>
                                   
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Descrição</label>

                                    <input type="text" required class="form-control     " id="desc_produto" name="desc_produto" value="<?php echo $produto[0]->desc_produto; ?>" >
                                    <?php echo form_error('mail_produto', '<div class="text-danger">', '</div>'); ?>
                                    
                                </div>
                               
                                <div class="form-group">
                                    <label>Grupo</label>
                                    <select class="form-control select2" id="os_id_categoria" name="id_categoria" onchange="os_busca_categoria($(this).val())" style="width: 100%;">
                                    <option value="<?php echo $produto[0]->id_categoria; ?>" ><?php echo $produto[0]->nm_categoria; ?></option>
                                    <?php if($this->session->userdata('categoria')){
                                        echo $this->session->userdata('categoria');
                                        foreach ($this->session->userdata('categoria') as $categorias) {?>
                                        
                                         <option value="<?php echo $categorias->id_categoria; ?>" ><?php echo $categorias->nm_categoria; ?></option>
                                           
                                      <?php  }} ?>

                                    } ?>
                                    <?php if ($categoria) {
                                        foreach ($categoria as $categorias) { ?>
                                    <option value="<?php echo $categorias->id_categoria; ?>" ><?php echo $categorias->nm_categoria; ?></option>
                                    <?php 
                                }
                            } ?>
                                     </select>
                                        
                              </div>   
                              
                 
                  
                  <div class="form-group">
                                <label>Sub Grupo</label>
                                <select class="form-control select2" id="os_id_setor"  name="id_subgrupo"  style="width: 100%;">
                                <option value="<?php echo $produto[0]->id_subcategoria; ?>" ><?php echo $produto[0]->nm_subcategoria; ?></option>
                               
                                </select>
                                        
                            </div> 
                



                               
                            
                            <div class="form-group">
                                    <label for="exampleInputEmail1">Código</label>

                                    <input type="text" class="form-control     " id="cod_sysdardani" name="cod_sysdardani" value="<?php echo $produto[0]->cod_sysdardani; ?>" >
                                    <?php echo form_error('nm_local', '<div class="text-danger">', '</div>'); ?>
                                </div>
                               
                                <div class="form-group">
                                <label>Unidade</label>
                                <select class="form-control      select2" id="unidade" name="unidade"  style="width: 100%;">
                                <option>UN</option>
                                <option>Kl</option>
                                </select>
                                        
                            </div>  
                              
                             
                              
                                
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Atualizar</button>
                                </div>
                            </div>      
                        </form>
                    </div>

                </div>


                </section>

            


            </div>

            <!-- /.content-wrapper -->




          