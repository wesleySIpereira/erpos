<div class="row">
    <?php
    get_msg('salvo');
    ?>
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title"><?php echo $titulo; ?></h2>
            </header>
            <div class="panel-body">
                <form role="form" method="post" id="formUsuarios" action="<?php echo base_url() . 'index.php/secretaria/update'; ?>">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nome</label>

                            <input type="text" class="form-control          " id="txtnome" name="nome" value="<?php echo $secretaria[0]->nm_secretaria; ?>" placeholder="Nome.">
                            <?php echo form_error('nome', '<div class="text-danger">', '</div>'); ?>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Secretario</label>

                            <input type="text" class="form-control          " id="txtnome" name="secretario" value="<?php echo $secretaria[0]->nm_secretario; ?>" placeholder="Nome.">
                            <?php echo form_error('secretario', '<div class="text-danger">', '</div>'); ?>
                        </div>
                        <input type="hidden" name="id_secretaria" value="<?php echo $secretaria[0]->id_secretaria; ?>">



                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Atualizar</button>
                        </div>
                </form>





            </div>
        </section>


    </div>