<div class="row">
	<div class="col-md-12">
		<?php
		get_msg('salvo');
		?>
		<form id="form1" method="post" id="formUsuarios" action="<?php echo base_url() . 'index.php/secretaria/save'; ?>" class="form-horizontal">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
						<a href="#" class="fa fa-caret-down"></a>
						<a href="#" class="fa fa-times"></a>
					</div>

					<h2 class="panel-title"><?php echo $titulo; ?></h2>
					<p class="panel-subtitle">

					</p>
				</header>
				<div class="panel-body">
					<div class="form-group">

						<label for="exampleInputEmail1">Nome</label>

						<input type="text" class="form-control     " id="txtnome" name="nome" value="<?php echo set_value('nome'); ?>">
						<?php echo form_error('nome', '<div class="text-danger">', '</div>'); ?>
					</div>

					<div class="form-group">
						<label for="exampleInputEmail1">Secretario</label>

						<input type="text" class="form-control     " id="txtnome" name="secretario" value="<?php echo set_value('secretario'); ?>">
						<?php echo form_error('secretario', '<div class="text-danger">', '</div>'); ?>
					</div>
				</div>
	</div>
	<footer class="panel-footer">
		<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Adicionar</button>

	</footer>
	</section>
	</form>
</div>
