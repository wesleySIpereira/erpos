
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" >
      <h1>
        subcategorias
        <small>Gerenciar subcategorias</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dasboard</a></li>
        <li class="active">subcategoria</li>
      </ol>
    </section>
    <hr>

    <!-- Main content -->
    <section class="content container-fluid">
    <?php
        get_msg('salvo');
        ?>
          <div class="row">
        <div class="col-xs-12">
          <div class="">
            <div class="">
            <a href="<?php echo base_url();?>index.php/subcategoria/novo" class="btn  btn-primary"><i class="fa fa-plus-circle"></i> Adicionar Sub Grupo</a>
              
            </div>
            <!--AQUI VAI ADIÇÃO FILTROS DE BUSCA -->  
          
            <div class="accordion-group widget-box">
                    <div class="accordion-heading">
                        <div class="widget-title "> <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse"> <span class="icon text-right"><i class="icon-list"></i></span>
                                <h5>Busca Avançada</h5>
                            </a> </div>
                    </div>
                    <div class="accordion-body <?php if($filtro){echo 'in';}else{echo '';} ?> collapse" id="collapseGOne" style="height: auto;">
                        <div class="widget-content">
                        <div class="box-body">

            <div class="row">
                <form action="" method="get">
                 
                 
               
                
                <!--aquiv vai mais um o nome p/ busca -->
                <div class="col-md-6">
                  <div class="form-group">
                                <label>Sub grupo</label>
                                <input type="text" class="form-control select2"   name="nm_subcategoria"  style="width: 100%;">       
                            </div> 
                </div>
                <div class="col-xs-offset-3">
               
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                                <label><br> </label>
                                <input type="submit" class="form-control select2 btn  <?php if($filtro){echo 'btn bg-purple';}else{echo 'btn btn-primary';} ?>" id="os_busca_nome"   name="buscar" value="<?php if($filtro){echo 'Desativar Filtro';}else{echo 'Ativar Filtro';} ?>" style="width: 100%;"> 
                            </div> 
                </div>
                
                
                
                </form>
                   </div>
                        </div>
                        </div>
                    </div>
            </div>
            <!--FINAL FILTROS DE BUSCA -->  
            <!-- /.box-header -->
             <!-- /.box-header -->
             <div class="widget-box">
                        <div class="widget-title"> <span class="icon"> <i class="fa fa-medkit"></i> </span>
                            <h5>Sub grupo</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered ">
                                
                                <thead>
                                    <tr style="backgroud-color: #2D335B">
                  <th>#id</th>
                  <th>Sub Grupo</th>
                  <th>Ações</th>
                </tr>
                </thead>
                <tbody style="text-align: center;">
                <tr>
                <?php if($subcategoria){ foreach ($subcategoria as $subcategorias) { ?>
                  <td><?php echo $subcategorias->id_subcategoria;?></td>
                  <td><?php echo $subcategorias->nm_subcategoria;?></td>
                  <td> <div class="btn-group-horiontal">
                      <a  data-toggle="modal" data-target="#ver_<?php echo $subcategorias->id_subcategoria; ?>" title="visualizar" class="btn btn-flat btn-default"><i class="fa fa-eye"></i></a>
                      <a href="<?php echo base_url(); ?>index.php/subcategoria/edit/<?php echo encript($subcategorias->id_subcategoria); ?>"  title="editar" class="btn btn-flat btn-info"><i class="fa fa-edit"></i></a>
                      <a data-toggle="modal" data-target="#modal-excluir_<?php echo $subcategorias->id_subcategoria; ?>" title="excluir"  class="btn btn-flat btn-danger"><i class="fa fa-trash"></i></a>
                    </div></td>
                </tr>
                <?php } }else{?>    
                
                <td colspan="3"><center>Nenhum cargo cadastrado</center> </td>
                </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                  <td colspan="9" style="align:right">    
                  <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">  
                  <ul class="pagination">
                  <?php if(isset($pag) && !empty($pag)){foreach ($pag as $key => $value) {
                      echo " {$value} ";
                  }}; ?>
                  </ul>
                  </div>
                  </td>
                    </tr>
                   
                </tfoot>
              </table>
                <!-- /.box-body -->
                <div class="box-footer">

                        </div>
                        <!-- /.box-footer-->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->








<!--modal visualizar detalhes -->


<!-- Button trigger modal -->


<!-- Modal -->
<?php
if ($subcategoria) {
    foreach ($subcategoria as $subcategorias) {
        ?>
        <div class="modal fade" id="ver_<?php echo $subcategorias->id_subcategoria; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>
                        
                    </div>
                    <div class="modal-body">
                   
                        <div class="text-info"> <b>Id:</b> <?php echo $subcategorias->id_subcategoria; ?><br>
                            <b>Cargo: </b><?php echo $subcategorias->nm_subcategoria; ?>
         <br> 
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default align-right" data-dismiss="modal"><i class="fa fa-close"></i> Fechar</button>

                    </div>
                </div>
            </div>
        </div>

        <?php
    }
}
?>  



<?php
if ($subcategoria) {
    foreach ($subcategoria as $subcategorias) {
        ?>
        <form action="<?php echo base_url() ?>index.php/subcategoria/delete/<?php echo encript($subcategorias->id_subcategoria); ?>" method="get" >
        <div class="modal fade" id="modal-excluir_<?php echo $subcategorias->id_subcategoria; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>
                        
                    </div>
                    <div class="modal-body">
                   
                        <div class="text-bold ">
                        <h4><center> Deseja realmente apagar esse registro?</center></h4>
         <br> 
                        </div>


                    </div>
                    <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <input type="submit" class="btn btn-danger" value="Excluir">

                    </div>
                </div>
            </div>
        </div>
        </form>    
        <?php
    }
}
?>  