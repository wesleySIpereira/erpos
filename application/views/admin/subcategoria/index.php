<div class="row">
    <script>
        function msg() {
            $('.mb-xs').trigger('click');
        }
    </script> 
    <?php
    get_msg('salvo');
    ?>
    <div class="col-md-12">
        <a href="<?php echo base_url(); ?>index.php/subcategoria/novo" class="btn btn-success "><i class="fa fa-plus-circle"></i> Adicionar Subcategoria</a>
        <br><br>
        <div class="panel-group" id="accordion">
            <div class="panel panel-accordion">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <a class="accordion-toggle  collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1One" aria-expanded="false">
                            Busca Avançada
                        </a>
                    </h6>
                </div>
                <div id="collapse1One" class="accordion-body <?php
                if ($filtro) {
                    echo 'in';
                } else {
                    echo '';
                }
                ?> collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <form action="" method="get">



                            <!--aquiv vai mais um o nome p/ busca -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Sub grupo</label>
                                    <input type="text" class="form-control select2"   name="nm_subcategoria"  style="width: 100%;">       
                                </div> 
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><br> </label>
                                    <input type="submit" class="form-control select2 btn  <?php if ($filtro) {
                    echo 'btn bg-purple';
                } else {
                    echo 'btn btn-primary';
                } ?>" id="os_busca_nome"   name="buscar" value="<?php if ($filtro) {
                    echo 'Desativar Filtro';
                } else {
                    echo 'Ativar Filtro';
                } ?>" style="width: 100%;"> 
                                </div> 
                            </div>



                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>

</div>

<section class="panel">

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-condensed mb-none">


                <th class="text-center" class="text-center">#id</th>
                <th class="text-center">Nome</th>

                <th class="text-center">Ações</th>
                </tr>
                </thead>
                <tbody style="text-align: center;">
                    <tr>
                    <?php if ($subcategoria) {
                        foreach ($subcategoria as $subcategorias) { ?>
                                <td><?php echo $subcategorias->id_subcategoria; ?></td>
                                <td><?php echo $subcategorias->nm_subcategoria; ?></td>

                                <td> <div class="btn-group-horiontal">
                                        <a  data-toggle="modal" data-target="#ver_<?php echo $subcategorias->id_subcategoria; ?>" title="visualizar" class="btn btn btn-xs btn-flat btn-default"><i class="fa fa-eye"></i></a>
                                        <a href="<?php echo base_url(); ?>index.php/subcategoria/edit/<?php echo encript($subcategorias->id_subcategoria); ?>"  title="editar" class="btn btn btn-xs btn-flat btn-info"><i class="fa fa-edit"></i></a>
                                        <a data-toggle="modal" data-target="#modal-excluir_<?php echo $subcategorias->id_subcategoria; ?>" title="excluir"  class="btn btn-flat btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
                                    </div></td>
                            </tr>
                        <?php }
                    } else { ?>    

                    <td colspan="4"><center>Nenhuma secretaria cadastrada</center> </td>
                    </tr>
<?php } ?>
                </tbody>
            </table>
            <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">  
                <ul class="pagination">
<?php
if (isset($pag) && !empty($pag)) {
    foreach ($pag as $key => $value) {
        echo " {$value} ";
    }
};
?>
            </div>
        </div>
</section>





<!-- modal visualização -->

<!-- Modal Info -->

<?php
if ($subcategoria) {
    foreach ($subcategoria as $subcategorias) {
        ?>
        <div class="modal fade modal-info " id="ver_<?php echo $subcategorias->id_subcategoria; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>

                    </div>
                    <div class="modal-body">

                        <div class="text-info"> <b>Id:</b> <?php echo $subcategorias->id_subcategoria; ?><br>
                            <b>Secretaria: </b><?php echo $subcategorias->nm_subcategoria; ?>
                            <br> 

                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default align-right" data-dismiss="modal"><i class="fa fa-close"></i> Fechar</button>

                    </div>
                </div>
            </div>
        </div>

        <?php
    }
}
?>  

<!-- modal realmente deletar -->
<?php
if ($subcategoria) {
    foreach ($subcategoria as $subcategorias) {
        ?>
        <form action="<?php echo base_url() ?>index.php/subcategoria/delete/<?php echo encript($subcategorias->id_subcategoria); ?>" method="get" >
            <div class="modal fade" id="modal-excluir_<?php echo $subcategorias->id_subcategoria; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>

                        </div>
                        <div class="modal-body">

                            <div class="text-bold ">
                                <h4><center> Deseja realmente apagar esse registro?</center></h4>
                                <br> 
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                            <input type="submit" class="btn btn-danger" value="Excluir">

                        </div>
                    </div>
                </div>
            </div>
        </form>    
        <?php
    }
}
?>  
