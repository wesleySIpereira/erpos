<div class="row">
    <script>
        function msg() {
            $('.mb-xs').trigger('click');
        }
    </script> 
    <?php
    get_msg('salvo');
    ?>
    <div class="col-md-12">


        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title"><?php echo $titulo; ?></h2>
                <p class="panel-subtitle">

                </p>
            </header>
            <form id="form1" method="post" id="formUsuarios" action="<?php echo base_url() . 'index.php/subcategoria/save'; ?>">
                <div class="panel-body">

                    <div class="form-group">
                        <label>GRUPO</label>
                        <select class="form-control      select2" name="cid_categoria" style="width: 100%;">
                            <option selected="selected">selecione</option>
                            <?php
                            if ($categoria) {
                                foreach ($categoria as $categorias) {
                                    ?>
                                    <option value="<?php echo $categorias->id_categoria; ?>" ><?php echo $categorias->nm_categoria; ?></option>
                                <?php }
                            }
                            ?>
                        </select>

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nome Sub Grupo</label>

                        <input type="text" class="form-control     " id="txtnome" name="nome" value="<?php echo set_value('nome'); ?>" >
<?php echo form_error('nome', '<div class="text-danger">', '</div>'); ?>
                    </div>



                </div>
                <footer class="panel-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Adicionar</button>

                </footer>
            </form>



        </section>
    </div>
</div>
