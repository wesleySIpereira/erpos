<!-- start: page -->

<h3 class="mt-none"></h3>
<p></p>
 <!--Atualiza a pagina a cada 3 minutos -->
 <script>
setTimeout(function() {
  window.location.reload(1);
}, 180000); // 3 minutos
</script>   
<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Abertas',  <?php echo $os_abertas; ?>],
          ['Resolvendo',     <?php echo $os_resolvendo; ?>],
          ['Finalizado',  <?php echo $os_fechado; ?>],
          ['Cancelado', <?php echo $os_cancelado; ?>],
          ['Parado',    <?php echo $os_parado; ?>]
        ]);

        var options = {
          title: 'Chamados',
          is3D: true,
          colors: ['#e0440e', '#d8b83f', 'green', '#dd4b39', 'silver'],

        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
     <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Abertas',  <?php echo $os_abertas_d; ?>],
          ['Resolvendo',     <?php echo $os_resolvendo_d; ?>],
          ['Finalizado',  <?php echo $os_fechado_d; ?>],
          ['Cancelado', <?php echo $os_cancelado_d; ?>],
          ['Parado',    <?php echo $os_parado_d; ?>]
        ]);

        var options = {
          title: 'Chamados Hoje',
          is3D: true,
          colors: ['#e0440e', '#d8b83f', 'green', '#dd4b39', 'silver'],

        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart-hoje'));

        chart.draw(data, options);
      }
    </script>
<div class="row">
    <div class="col-md-4 col-lg-3 col-xl-3">
        <section class="visible panel panel-featured-left  panel-featured-primary-md">
            <div class="panel-body">
                <div class="widget-summary widget-summary-md">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-primary">
                            <i class="fa fa-life-ring"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title">Total de Chamados</h4>
                            <div class="info">
                                <strong class="amount"><?php echo $os; ?></strong>
                                <a href="<?php echo base_url(); ?>index.php/os" class="link text-uppercase">(Detalhes)</a>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <a href="<?php echo base_url(); ?>index.php/os" class="text-muted text-uppercase">(Detalhes)</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-3 col-lg-3 col-xl-3">
        <section class="visible panel panel-featured-left panel-featured-primary-md">
            <div class="panel-body">
                <div class="widget-summary widget-summary-md">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-quartenary">
                            <i class="fa fa-users"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title">Total de Funcionários</h4>
                            <div class="info">
                                <strong class="amount"><?php echo $funcionario - 1; ?></strong>
                                <a  href="<?php echo base_url(); ?>index.php/funcionario" class="link text-uppercase">(Detalhes)</a>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <a class="text-muted text-uppercase">(Detalhes)</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-3 col-lg-3 col-xl-3">
        <section class="visible panel panel-featured-left panel-featured-primary-md">
            <div class="panel-body">
                <div class="widget-summary widget-summary-md">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-info">
                            <i class="fa  fa-user-md"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title">Total equipe Técnica</h4>
                            <div class="info">
                                <strong class="amount"><?php echo $tecnico - 1; ?></strong>
                                <a href="<?php echo base_url(); ?>index.php/tecnico" class="text-muted text-uppercase">(Detalhes)</a>
                            </div>
                        </div>
                        <div class="summary-footer">
                           
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-3 col-lg-3 col-xl-3">
        <section class="visible panel panel-featured-left panel-featured-primary-md">
            <div class="panel-body">
                <div class="widget-summary widget-summary-md">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-warning">
                            <i class="fa fa-building"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title">Total de Secretárias</h4>
                            <div class="info">
                                <strong class="amount"><?php echo $secretaria; ?></strong>
                                <a href="<?php echo base_url(); ?>index.php/secretaria" class="text-muted text-uppercase">(Detalhes)</a>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <a class="text-muted text-uppercase">(Detalhes)</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</div>
<!-- google chart -->
<div class="row">
    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">Estatísticas todos  Chamados</h2>
            </header>
            <div class="panel-body">
            <div id="piechart" class="col-lg-12" style="width:px; height: 300px;">
            </div>
        </section>



    </div>
    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">Estatísticas Chamados Hoje  (<?php echo date('d/m/Y'); ?>)</h2>
            </header>
            <div class="panel-body">
            <div id="piechart-hoje" class="col-lg-12" style="width:px; height: 300px;">
            </div>
        </section>



    </div>
</div>