<div class="row">
<script>
function msg(){
 $('.mb-xs').trigger('click');
}
</script> 
<?php
        get_msg('salvo');
        ?>
    <div class="col-md-12">

        <form id="form1" method="post" id="formUsuarios" action="<?php echo base_url() . 'index.php/equipamento/save'; ?>">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>

                    <h2 class="panel-title"><?php echo $titulo; ?></h2>
                    <p class="panel-subtitle">

                    </p>
                </header>
                <div class="panel-body">
                   
                    
                      <div class="form-group">

                        <label for="exampleInputEmail1">Patrimônio</label>

                        <input type="text" class="form-control     " id="txtnome" name="n_patrimonio" value="<?php echo set_value('n_patrimonio'); ?>">
                        <?php echo form_error('n_patrimonio', '<div class="text-danger">', '</div>'); ?>
                    </div>
                     <div class="form-group">

                        <label for="exampleInputEmail1">Nome</label>

                        <input type="text" class="form-control     " id="txtnome" name="nm_equipamento" value="<?php echo set_value('nm_equipamento'); ?>" placeholder="Nome.">
                        <?php echo form_error('nm_equipamento', '<div class="text-danger">', '</div>'); ?>
                    </div>
                    
                     <div class="form-group">
                        <label>Categoria</label>
                        <select class="form-control      select2" id="id_secretaria" name="cid_cat_equip" onchange="busca_setor($(this).val())" style="width: 100%;">
                            <option></option>
                            <?php if ($cequipamento) {
                                foreach ($cequipamento as $cequipamentos) { ?>
                                    <option value="<?php echo $cequipamentos->id_cat_equip; ?>"><?php echo $cequipamentos->nm_cat_equip; ?></option>
                            <?php
                                }
                            } ?>
                        </select>
                    </div>
                     <div class="form-group">
                        <label>Marca</label>
                        <select class="form-control      select2" id="id_secretaria" name="cid_marc_equip" onchange="busca_setor($(this).val())" style="width: 100%;">
                            <option></option>
                            <?php if ($mequipamento) {
                                foreach ($mequipamento as $mequipamentos) { ?>
                                    <option value="<?php echo $mequipamentos->id_marc_equip; ?>"><?php echo $mequipamentos->nm_marc_equip; ?></option>
                            <?php
                                }
                            } ?>
                        </select>
                    </div>
                     <div class="form-group">

                        <label for="exampleInputEmail1">Descrição</label>

                        
                        
                        <textarea class="col-lg-12 form-control " id="txtnome" name="desc_equipamento" value="<?php echo set_value('desc_equipamento'); ?>" cols="30" rows="5"></textarea>
                        <?php echo form_error('nome', '<div class="text-danger">', '</div>'); ?>
                     </div>
                      <div class="form-group">

                        <label for="exampleInputEmail1">Nome na Rede</label>

                        <input type="text" class="form-control     " id="txtnome" name="nm_rede" value="<?php echo set_value('nm_rede'); ?>" placeholder="Nome.">
                        <?php echo form_error('nome', '<div class="text-danger">', '</div>'); ?>
                    </div>
                      <div class="form-group">

                        <label for="exampleInputEmail1">Endereço Mac</label>

                        <input type="text" class="form-control     " id="txtnome" name="n_mac" value="<?php echo set_value('n_mac'); ?>" placeholder="Nome.">
                        <?php echo form_error('nome', '<div class="text-danger">', '</div>'); ?>
                    </div>
                       <div class="form-group">
                        <label>Secretaria</label>
                        <select class="form-control      select2" id="id_secretaria" name="cid_secretaria" onchange="busca_setor($(this).val())" style="width: 100%;">
                            <option></option>
                            <?php if ($secretaria) {
                                foreach ($secretaria as $secretarias) { ?>
                                    <option value="<?php echo $secretarias->id_secretaria; ?>"><?php echo $secretarias->nm_secretaria; ?></option>
                            <?php
                                }
                            } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Setor</label>
                        <select class="form-control      select2" id="id_setor" name="id_setor" style="width: 100%;">


                        </select>
                    </div>

                 
                </div>
                <footer class="panel-footer">
        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Adicionar</button>

    </footer>
                 </section>
                </form>

    </div>

    
   
</div>
