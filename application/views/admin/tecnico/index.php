<div class="row">
<script>
function msg(){
 $('.mb-xs').trigger('click');
}
</script> 
<?php
        get_msg('salvo');
        ?>
    <div class="col-md-12">
        <a href="<?php echo base_url(); ?>index.php/tecnico/novo" class="btn btn-success "><i class="fa fa-plus-circle"></i> Adicionar Técnico</a>
        <br><br>
        <div class="panel-group" id="accordion">
            <div class="panel panel-accordion">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <a class="accordion-toggle  collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1One" aria-expanded="false">
                            Busca Avançada
                        </a>
                    </h6>
                </div>
                <div id="collapse1One" class="accordion-body <?php if ($filtro) {
                                                                    echo 'in';
                                                                } else {
                                                                    echo '';
                                                                } ?> collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <form action="" method="get">



                            <!--aquiv vai mais um o nome p/ busca -->
                            <div class="col-md-4">
                                <div class="form-group">
                                <label>Técnico</label>
                                <input type="text" class="form-control select2" id="os_busca_nome"   name="busca_nome"  style="width: 100%;">       
                                </div>
                            </div>
                         
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><br> </label>
                                    <input type="submit" class="form-control select2 btn <?php if ($filtro) {
                                                                                                echo 'btn bg-quartenary';
                                                                                            } else {
                                                                                                echo 'btn btn-info';
                                                                                            } ?>" id="os_busca_nome" name="buscar" value="<?php if ($filtro) {
                                                                                                                                                echo 'Desativar Filtro';
                                                                                                                                            } else {
                                                                                                                                                echo 'Ativar Filtro';
                                                                                                                                            } ?>" style="width: 100%;">
                                </div>
                            </div>



                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>

</div>

<section class="panel">

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-condensed mb-none">


              
                  <th class="text-center">#id</th> 
                  <th class="text-center">Funcionário</th>
                  <th class="text-center">Resp</th>
                  <th class="text-center">Cargo</th>
                  <th class="text-center">Secretaria</th>
                  <th class="text-center">Setor</th>
                  <th class="text-center">Ramal/Telefone</th>
                 
                  <th class="text-center">Ações</th>
                
                </thead>
                <tbody style="text-align: center;">
                    <tr>
                        <?php if ($tecnico) {
                            foreach ($tecnico as $tecnicos) { 
                                /* NÃO MOSTRAR O PRIMEIRO TECNICO POIS É ADMINISTRADOR */
                                
                                if($tecnicos->id_tecnico <>'1'){?>
                            
                                <td><?php echo $tecnicos->id_tecnico; ?></td>
                                <td><?php echo $tecnicos->nm_funcionario; ?></td>
                                <td><?php echo $tecnicos->n_resp; ?></td>
                                <td><?php echo $tecnicos->nm_cargo; ?></td>
                                <td><?php echo $tecnicos->nm_secretaria; ?></td>
                                <td><?php echo $tecnicos->nm_setor; ?></td>
                                <td><?php echo $tecnicos->n_ramal; ?></td>
                                <td>
                                    <div class="btn-group-horiontal">
                                        <a data-toggle="modal" data-target="#ver_<?php echo $tecnicos->id_tecnico; ?>" title="visualizar" class="btn btn-flat btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                        <a href="<?php echo base_url(); ?>index.php/tecnico/edit/<?php echo encript($tecnicos->id_tecnico); ?>" title="editar" class="btn btn btn-xs btn-flat btn-info"><i class="fa fa-edit"></i></a>
                                        <a data-toggle="modal" data-target="#modal-excluir_<?php echo $tecnicos->id_tecnico; ?>" title="excluir" class="btn btn-flat btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                </td>
                    </tr>
                <?php }}
                        } else { ?>

                <td colspan="4">
                    <center>Nenhuma secretaria cadastrada</center>
                </td>
                </tr>
            <?php } ?>
                </tbody>
            </table>
            <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                <ul class="pagination">
                    <?php if (isset($pag) && !empty($pag)) {
                        foreach ($pag as $key => $value) {
                            echo " {$value} ";
                        }
                    }; ?>
            </div>
        </div>
</section>





<!-- modal visualização -->

<!-- Modal Info -->

<!-- Modal-->
<?php
if ($tecnico) {
    foreach ($tecnico as $tecnicos) {
        ?>
        <div class="modal fade" id="ver_<?php echo $tecnicos->id_tecnico; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>
                        
                    </div>
                    <div class="modal-body">
                   
                        <div class="text-info"> <b>Id:</b> <?php echo $tecnicos->id_tecnico; ?><br>
                            <b>Funcionário: </b><?php echo $tecnicos->nm_funcionario; ?>
                            <br> 
                            <b>Nº Resp: </b><?php echo $tecnicos->n_resp; ?>
                            <br> 
                            <b>E-mail: </b><?php echo $tecnicos->mail_funcionario; ?>
                            <br> 
                            <b>Cargo: </b><?php echo $tecnicos->nm_cargo; ?>
                            <br> 
                            <b>Nome de Usuário do Sistema: </b><?php echo $tecnicos->usuario; ?>
                            <br> 
                            <b>Secretaria: </b><?php echo $tecnicos->nm_secretaria; ?>
                            <br> 
                            <b>Setor: </b><?php echo $tecnicos->nm_setor; ?>
                            <br> 
                            <b>Local: </b><?php echo $tecnicos->nm_local; ?>
                            <br>
                            <b>Obs local de trabalho: </b><?php echo $tecnicos->obs_local; ?>
                            <br> 
                            <b>Responsável do Setor: </b><?php echo $tecnicos->nm_responsavel; ?>
                            <br> 
                            <b>Nº Ramal/Telefone: </b><?php echo $tecnicos->n_ramal; ?>
                            <br> 
                            <b>Status: </b><?php if($tecnicos->status){echo 'ativo';}else{echo 'inativo';} ?>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default align-right" data-dismiss="modal"><i class="fa fa-close"></i> Fechar</button>

                    </div>
                </div>
            </div>
        </div>

        <?php
    }
}
?>  



<!-- modal realmente deletar -->
<?php
if ($tecnico) {
    foreach ($tecnico as $tecnicos) {
        ?>
        <form action="<?php echo base_url(); ?>index.php/tecnico/delete/<?php echo encript($tecnicos->id_tecnico);?>/<?php echo encript($tecnicos->fid_funcionario);?>/<?php echo encript($tecnicos->cid_local);?>" method="get" >
        <div class="modal fade" id="modal-excluir_<?php echo $tecnicos->id_tecnico; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-bold text-info" id="myModalLabel">ERPOS  </h4>
                        
                    </div>
                    <div class="modal-body">
                   
                        <div class="text-bold ">
                        <h4><center> Deseja realmente apagar esse registro?</center></h4>
         <br> 
                        </div>


                    </div>
                    <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <input type="submit" class="btn btn-danger" value="Excluir">

                    </div>
                </div>
            </div>
        </div>
        </form>    
        <?php
    }
}
?>  