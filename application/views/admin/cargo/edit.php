<div class="row">
    <?php
    get_msg('salvo');
    ?>
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title"><?php echo $titulo; ?></h2>
            </header>
            <div class="panel-body">
                <form role="form" method="post" id="formUsuarios" action="<?php echo base_url() . 'index.php/cargo/update'; ?>">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nome</label>

                            <input type="text" class="form-control     " id="txtnome" name="nm_cargo" value="<?php echo $cargo1[0]->nm_cargo; ?>" placeholder="Nome.">
                            <?php echo form_error('nome', '<div class="text-danger">', '</div>'); ?>
                        </div>
                        <input type="hidden" name="id_cargo" value="<?php echo $cargo1[0]->id_cargo; ?>">

                    </div>



                    <br>

                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Atualizar</button>
                    </div>
                </form>





            </div>
        </section>


    </div>