<?php
class Tecnico_model extends CI_Model {

    /**
     * author: Wesley da Silva Pereira
     * email: wesley_cras@hotmail.com
     * 
     */
    
    function __construct() {
        parent::__construct();
    }

    
    function get($table,$fields,$where='',$join='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields);
        $this->db->from($table);
       // $this->db->order_by('id_categoria','asc');
       
        $this->db->limit($perpage,$start);
        if($join){
           $this->db->join($join);
        }
       
        
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    function getClientes($id){
        $this->db->where('id',$id);
        $this->db->limit(1);
        return $this->db->get('clientes')->row();
    }
    
    function add($table,$data){
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
		{
			$negociacao_id = $this->db->insert_id();
			return $negociacao_id;
		}
		
		return FALSE;       
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    }

    function count($table) {
        return $this->db->count_all($table);
    }
    
    public function getCat($id){
        $this->db->where('id_categoria',$id);
        $this->db->order_by('id_categoria','desc');
       // $this->db->limit(10);
        return $this->db->get('tb_categoria')->result();
    }
    function getMarca($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('id_marca','asc');
        $this->db->limit($perpage,$start);
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
    function getCategoria($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('id_categoria','asc');
        $this->db->limit($perpage,$start);
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
    public function verificaUnicidade($table,$fields,$where){
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get();
        return $query->row();
    }
    function ValidaLogin($table,$fields,$where)
	{

		$this->db->select($fields);
        $this->db->from($table);
       // $this->db->order_by('id_categoria','asc');
       
        
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  $query->result();
        return $result;
		
        }	
        public function listaTecnicoLike($pular=null,$row_perpage=null,$table,$fields,$like=null,$where=null,$order=null){
     
            $this->db->select($fields);
                $this->db->from($table);
                $this->db->limit($row_perpage,$pular);
            
                $this->db->order_by('id_tecnico','desc');
            
            if($where){
                $this->db->where($where);
            }
            if($like){
                $this->db->like($like,'before');
            }
            $this->db->where(array('status'=>1));
        
            $query = $this->db->get();
                
            $result =   $query->result();
                return $result;
        
          }    
      
          public function listaTecnicoWhere($table,$fields,$where=null){
            $this->db->select($fields);
            $this->db->from($table);
            $this->db->order_by('id_tecnico','desc');
            if($where){
                $this->db->where($where);
            }
            $query = $this->db->get();
                
            $result =   $query->result();
                return $result;
          }
}