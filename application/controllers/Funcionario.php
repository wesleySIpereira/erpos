<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Funcionario extends CI_Controller
{

    /**
     * 
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('funcionario/funcionario_model', 'Mfuncionario');
        $this->load->model('secretaria/secretaria_model','MSecretaria');
        $this->load->model('os/os_model','Mos');
        $this->load->library('form_validation');
       
    }

    public function index()
    {
        //index manda para funcão principal gerenciar
        if(!$this->session->userdata('id_funcionario')){
      
			
			set_msg('salvo', '<center><i class="fa fa-warning text-danger"></i> <label class="text-danger text-center">Você precisa estar logado para acessar.</label></center>', 'sucesso');
			redirect('usuario', 'refresh');
			 
		   }else{
                $this->gerenciar();
           }

    }
    public function gerenciar()
    {


          /*
        
        */
        $filtro=false;   
        //verifica se existe condição de busca #endregion
     if(isset($_GET['id_secretaria'])&& !empty($_GET['id_secretaria']) or (isset($_GET['id_setor']) && !empty($_GET['id_setor']) or (isset($_GET['cid_cargo'])&& !empty($_GET['cid_cargo'])) or (isset($_GET['busca_nome'])&& !empty($_GET['busca_nome'])) )){   
        $url='';
        
        if(isset($_GET['id_secretaria']) && !empty($_GET['id_secretaria'])){
            $filtro=true;
            $url='&id_secretaria='.$_GET['id_secretaria'].'&id_setor='.$_GET['id_setor'].'&cid_cargo='.$_GET['cid_cargo'].'&busca_nome='.$_GET['busca_nome'];
            $like['cid_secretaria']=$_GET['id_secretaria'];  
        }
        if(!empty($_GET['id_setor'])){
            $filtro=true;
            $url=$url.'&id_setor='.$_GET['id_setor'];
            $like['id_setor']=$_GET['id_setor'];
        }
        if(!empty($_GET['cid_cargo'])){
            $filtro=true;
            $url=$url.'&cid_cargo='.$_GET['cid_cargo'];
            $like['cid_cargo']=$_GET['cid_cargo'];
        }
        if(!empty($_GET['busca_nome'])){
            $filtro=true;
            $url=$url.'&busca_nome='.$_GET['busca_nome'];
            $like['nm_funcionario']=$_GET['busca_nome'];
        }
       
       
    }else{
        $like=null;
        $url='';
    }  
        

         //primeiro passas as  configuraçoes para pagination

         $data['secretaria'] = $this->Mfuncionario->get('tb_secretaria', '*', $where = '',$join='', $config['per_page']=null, $this->uri->segment(3));
         $p=0;//inicio do contador da paginação
         $pg=1;
         $total_registros= count($this->Mfuncionario->listaFuncionariosLike($p=0,$por_pagina=null,'v_funcionario','*',$like,$order=null));//pegar total registros
         $per_page=5;//numero de registros por paginas;
         $paginas=$total_registros/$per_page;
     
        
 
 
 
         if(isset($_GET['p']) && !empty($_GET['p'])){
             $pg=addslashes($_GET['p']);
         }
         $p=($pg-1)*$per_page;

        $data['funcionario'] = $this->Mfuncionario->listaFuncionariosLike($p,$per_page,'v_funcionario','*',$like,$order=null);
        $data['cargo1'] = $this->Mfuncionario->get('tb_cargo', '*', $where = '', $config['per_page']=null, $this->uri->segment(3));//padrao de busca   
        //$data['secretaria'] = $this->MSecretaria->listaOsLike($p,$per_page,'tb_secretaria','*',$like,$order=null);
        $data['filtro']=$filtro;
        $data['pag']=paginacao($total_registros,$per_page,$pg,$url);
        
        $data['titulo']='Funcionário';
        $data['view']='admin/funcionario/index';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
		$data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $data['jscript']='theme/footer';
        $this->load->view('theme/header',$data);
       // $this->load->view('theme/header');
       // $this->load->view('admin/funcionario/index', $data);
       // $this->load->view('theme/footer');

    }
    public function novo()
    {
       //função que chama o formulario para adição de novo funcionario
        $data = null;
        $config['per_page'] = null;
        $data['cargo1'] = $this->Mfuncionario->get('tb_cargo', '*', $where = '', $config['per_page'], $this->uri->segment(3));
        $data['secretaria'] = $this->Mfuncionario->get('tb_secretaria', '*', $where = '', $config['per_page'], $this->uri->segment(3));
        $data['titulo']='Adicionar Funcionário';
        $data['view']='admin/funcionario/add';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['jscript']='theme/footer';
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        
        $this->load->view('theme/header',$data);
    }
    public function save()
    {
       
   
       
            


            $nome = $this->input->post('nome');
            $nm_local = $this->input->post('nm_local');
            $id_setor = $this->input->post('id_setor');
            $n_ramal = $this->input->post('n_ramal');
            $obs_local = $this->input->post('obs_local');
            $cid_cargo = $this->input->post('cid_cargo');
            $cid_secretaria = $this->input->post('cid_secretaria');
            $mail_funcionario = $this->input->post('mail_funcionario');
            $n_resp= $this->input->post('n_resp');
            $nm_usuario=$this->input->post('nm_usuario');
            $senha=$this->input->post('senha');
            //monta o array de dados

            $dlocal = array(
                'cid_setor' => $id_setor,
                'nm_local' => $nm_local,
                'n_ramal' => $n_ramal,
                'obs_local' => $obs_local,
                'cid_sec'=>$cid_secretaria

            );

            if ($this->Mfuncionario->verificaUnicidade('tb_funcionario', 'nm_funcionario', $where = ['nm_funcionario' => $nome])) {
                 //Verifica se já existe esse dada cadastrado no banco de dados
                set_msg('salvo', '<div class="alert alert-warning alert-dismissible">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                 <h4><i class="icon fa fa-check"></i> Atenção!</h4>
                 Esse funcionario já esta cadastrado.
               </div>', 'sucesso');
                redirect('funcionario/novo', 'refresh');
            } else {
                //salva primeiro o local de trabalho
                $id_local = $this->Mfuncionario->add('tb_local_trabalho', $dlocal);//salva local de trabalho e retorna o id inserido
                
                /* cria os dados pra serem inseridos no novo funcionar junto com o id do local */
                $dados = array(
                    'cid_local'=>$id_local,
                    'cid_cargo'=>$cid_cargo,
                    'mail_funcionario'=>$mail_funcionario,
                    'n_resp'=>$n_resp,
                    'nm_funcionario'=>$nome,
                    'nm_usuario'=>$nm_usuario,
                    'senha'=>$senha,
                    'ativo'=>1
                );
                //dispara email com confirmação
                $this->envia_email($nm_usuario,$senha,$mail_funcionario,$nome);
                $this->Mfuncionario->add('tb_funcionario', $dados);
                //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                   funcionario adicionado com sucesso.
                 </div>', 'sucesso');
                redirect('funcionario', 'refresh');
            }



       
    }
    public function edit()
    {
      //pega parametro da url via get
        $id = decrypt($this->uri->segment(3));
       //monta o like da busca p/ editar
       $like=array(
           'id_funcionario'=>$id
       );
      //primeira coisa fazer um select com id
        $config['per_page'] = null;
        $data['cargo1'] = $this->Mfuncionario->pegaDados('tb_cargo');
        $data['secretaria'] = $this->Mfuncionario->pegaDados('tb_secretaria');
      //  $data['funcionario'] = $this->Mfuncionario->get('tb_funcionario', '*', $where = ['id_funcionario' => $id], $config['per_page'], null);
       $data['funcionario'] = $this->Mfuncionario->getFuncionariolike('v_funcionario','*',$like,$join='',$perpage=0,$start=0,$one=false,$array='array');
       $data['titulo']='Editar Funcionário';
       $data['view']='admin/funcionario/edit';//passa a view por padrao
       $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
       $data['jscript']='theme/footer';
       $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
       
       $this->load->view('theme/header',$data);

    }

    public function update($id)
    {
        
          //pega os dados via 
          $id_funcionario = decrypt($this->uri->segment(3));
          $nome = $this->input->post('nome');
          $nm_local = $this->input->post('nm_local');
          $id_setor = $this->input->post('id_setor');
          $n_ramal = $this->input->post('n_ramal');
          $obs_local = $this->input->post('obs_local');
          $cid_cargo = $this->input->post('cid_cargo');
          $cid_secretaria = $this->input->post('cid_secretaria');
          $mail_funcionario = $this->input->post('mail_funcionario');
          $n_resp= $this->input->post('n_resp');
          $nm_usuario=$this->input->post('nm_usuario');
          $senha=$this->input->post('senha');
          $id_local=$this->input->post('id_local');
          $id_setor_old=$this->input->post('id_setor_old');
          //monta o array de dados
        
         
        
          $dlocal = array(
              'cid_setor' => $id_setor,
              'nm_local' => $nm_local,
              'n_ramal' => $n_ramal,
              'obs_local' => $obs_local,
              'cid_sec'=>$cid_secretaria

          );


                //salva primeiro o local de trabalho
            
                
            /* cria os dados pra serem inseridos no novo funcionar junto com o id do local */
            $dados = array(
                'cid_local'=>$id_local,
                'cid_cargo'=>$cid_cargo,
                'mail_funcionario'=>$mail_funcionario,
                'n_resp'=>$n_resp,
                'nm_funcionario'=>$nome,
                'nm_usuario'=>$nm_usuario,
                'senha'=>$senha,
                'ativo'=>1,
                
            );
            $dsetor=array(
               'cid_secretaria'=>$cid_secretaria

            );


                 
                $this->Mfuncionario->edit('tb_funcionario', $dados, 'id_funcionario', $id);
              //  $this->Mfuncionario->edit('tb_setor', $dsetor,'id_setor',$id_setor_old);
                $this->Mfuncionario->edit('tb_local_trabalho', $dlocal,'id_local',$id_local);//salva local de trabalho e retorna o id inserido
                
                
                
                //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                funcionario Atualizado.
              </div>', 'sucesso');
               redirect('funcionario', 'refresh');
            }



     
   

    
    public function delete()
    {
        $id = decrypt($this->uri->segment(3));
        $id_local= decrypt($this->uri->segment(4));
     //deletar simples sem confirmação por hora
        $data=array(
            'ativo'=>0
        );
        $this->Mfuncionario->edit('tb_funcionario',$data,'id_funcionario',$id);//Somente desativar funcionário
       // $this->Mfuncionario->delete('tb_local_trabalho', 'id_local', $id_local);
       
        set_msg('salvo', '<div class="alert alert-info alert-dismissible">
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
     <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
     Por motivo de segurança o registro foi marcado como inativo.
   </div>', 'sucesso');
        redirect('funcionario', 'refresh');

    }
    public function get_setor()
    {
        $id_setor = $this->input->post('cid_secretaria');
        $id_setor;
        echo $this->Mfuncionario->geraSelect('tb_setor', $where = ['cid_secretaria' => $id_setor]);

    }
    public function verificaUnicidade(){
       // var_dump($this->input->post());
        
        $where=array(
            'nm_funcionario'=>$this->input->post('nome'),
            'mail_funcionario'=>$this->input->post('mail_funcionario'),
            'nm_usuario'=>$this->input->post('nm_usuario')
        );
       $funcionario=$this->Mfuncionario->verificaUnicidade('tb_funcionario','*',$where);
      
            
       if(isset($funcionario) and ($funcionario)<>null){
         echo '0';
     }else{
         echo '1';
     }
    }
    public function verificaEmailExiste(){
        $where=array(
            'mail_funcionario'=>$this->input->post('mail_funcionario')
        );
       
        $funcionario=$this->Mfuncionario->verificaUnicidade('tb_funcionario','*',$where);
      
            
       if(isset($funcionario) and ($funcionario)<>null){
         echo '0';
     }else{
         echo '1';
     }
    }

  public function verificaNumeroResp(){
    $where=array(
        'n_resp'=>$this->input->post('n_resp')
    );
   
    $funcionario=$this->Mfuncionario->verificaUnicidade('tb_funcionario','*',$where);
  
        
   if(isset($funcionario) and ($funcionario)<>null){
     echo '0';
 }else{
     echo '1';
 }
} 

public function verificaUsuario(){
    $where=array(
        'nm_usuario'=>$this->input->post('nm_usuario')
    );
   
    $funcionario=$this->Mfuncionario->verificaUnicidade('tb_funcionario','*',$where);
  
        
   if(isset($funcionario) and ($funcionario)<>null){
     echo '0';
 }else{
     echo '1';
 }

}

public function envia_email($usuario=null,$senha=null,$email=null,$nome){
    //ao termino de adicionar uma nova ordem de serviço é disparado um email.
    
    
    
    
     $this->load->library('email');
    
       $subject = 'Prefeitura Municipal de Patrocínio| CPD-chamados';
       $message = '<div class="tudo">
    <h3>CPD- Prefeitura Municipal de Patrocínio</h3>
    <br><p> Olá <b>'.$nome.'</b> sua conta foi criada.</p>
    <p>Agora você poderá criar e acompanhar seus pedidos de suporte ao CPD da Prefeitura.</p>
    <div class="balao">
    
    <table>
    <tr>
    <td ><p><b>Seus dados de acesso</b></p></td>
    </tr>
    <tr>
    <td><p><b>Usuário:</b> '.$usuario.'</p></td>
    </tr>
    <tr>
    <td><p><b>Senha:</b> '.$senha.'</p></td>
    </tr>
    </table>
    
    </div>
    <br>
    <br>
    <hr>
    <p>Qualquer dúvida entre em contato conosco 3839-1801 ramais 476,213 ou 500.</p>
    <p><b>CPD- Prefeitura de Patrocinio Minas Gerais.</b></p>
    <p><small>cpdpmp@patrocinio.mg.gov.br</small>.</p>
    </div>';
        
        $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
        <title>' . html_escape($subject) . '</title>
        <style type="text/css">
        body {
            font-family: Arial, Verdana, Helvetica, sans-serif;
            font-size: 16px;
        }
       
    .balao{
        border: 2px solid #605ca8;
        background-color: silver;
        padding:1%;
        color:#5C2582;
    }
    .tudo{
        background-color:white;
        color:black;
        padding:1%;
        border:1px solid #605ca8;
      
        
        </style>
    </head>
    <body>
    ' . $message . '
    </body>
    </html>';
    
    $result = $this->email
        ->from('suporte@wepsistemas.net.eu.org')
        ->reply_to('yoursecondemail@somedomain.com')    // Optional, an account where a human being reads.
        ->to($email)
        ->subject($subject)
        ->message($body)
        ->send();
    
    
    
    
    
    }
   
  
    

}//fim php




