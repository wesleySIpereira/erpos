<?php

/* 
 * Criador: Wesley da silva pereira 11-08-2020
 * Controler equipamentos 
 * aqui vão ser tratados todos os equipamentos cadastrados no cpd
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Equipamento extends CI_Controller {

	
    public function __construct() {
        parent::__construct();
        
        $this->load->model('cargo/Cargo_model','MCargo');
        $this->load->model('secretaria/secretaria_model','MSecretaria');
        $this->load->model('funcionario/funcionario_model', 'Mfuncionario');
        $this->load->model('equipamento/equipamento_model','Mequipamento');
        $this->load->model('cequipamento/cequipamento_model','MCequipamento');
        $this->load->model('mequipamento/mequipamento_model','MMequipamento');
        $this->load->library('form_validation');
        if(!$this->session->userdata('id_funcionario')){
      
			
			set_msg('salvo', '<center><i class="fa fa-warning text-danger"></i> <label class="text-danger text-center">Você precisa estar logado para acessar.</label></center>', 'sucesso');
			redirect('usuario', 'refresh');
			 
		   }
    }

	public function index()
	{
        //index manda para funcão principal gerenciar
      $this->gerenciar();
        
    }
    public function gerenciar(){

          //filtro nova os
          /*
        
        */
        $filtro=false;   
        //verifica se existe condição de busca #endregion
       
       // ["busca_nome"] ["id_secretaria"]["id_setor"] ["buscar"] "Ativar Filtro" } 
     if(isset($_GET['n_patrimonio']) && !empty($_GET['n_patrimonio']) or (isset($_GET['id_secretaria']) && !empty($_GET['id_secretaria']) or (isset($_GET['id_setor']) && !empty($_GET['id_setor']))) ){   
        $url='';
        
        if(isset($_GET['n_patrimonio']) && !empty($_GET['n_patrimonio'])){
            $filtro=true;
            $url='&n_patrimonio='.$_GET['n_patrimonio'];
            $like['n_patrimonio']=$_GET['n_patrimonio'];  
        }
        if(isset($_GET['id_secretaria']) && !empty($_GET['id_secretaria'])){
            $filtro=true;
            $url='&id_secretaria='.$_GET['id_secretaria'];
            $like['id_secretaria']=$_GET['id_secretaria'];  
        }
         if(isset($_GET['id_setor']) && !empty($_GET['id_setor'])){
            $filtro=true;
            $url='&id_setor='.$_GET['id_setor'];
            $like['id_setor']=$_GET['id_setor'];  
        }
        
       
       
    }else{
        $like=null;
        $url='';
    }
    
        //primeiro passas as  configuraçoes para pagination

     //$data['secretaria'] = $this->Mfuncionario->get('tb_secretaria', '*', $where = '',$join='', $config['per_page']=null, $this->uri->segment(3));
     $p=0;//inicio do contador da paginação
     $pg=1;
     $total_registros= count($this->Mequipamento->listaCargoLike($p=0,$por_pagina=null,'v_equipamento','*',$like,$order=null));//pegar total registros
     $per_page=5;//numero de registros por paginas;
     $paginas=$total_registros/$per_page;
       
     if(isset($_GET['p']) && !empty($_GET['p'])){
        $pg=addslashes($_GET['p']);
    }
    $p=($pg-1)*$per_page;
       
 


        //primeiro busca todos os dados no banco 
        $data['filtro']=$filtro;
        $data['equipamento'] = $this->Mequipamento->listaCargoLike($p,$per_page,'v_equipamento','*',$like,$order=null);
        $data['secretaria'] = $this->Mfuncionario->get('tb_secretaria', '*', $where = '',$join='', $config['per_page']=null, $this->uri->segment(3));
        $data['pag']=paginacao($total_registros,$per_page,$pg,$url);
        $data['jscript']='theme/footer';
        $data['titulo']='Equipamento';
        $data['view']='admin/equipamento/index';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
            $this->load->view('theme/header',$data);

    }
    public function novo(){
       //função que chama o formulario para adição de novo cargo
       $data = null;
      $data['titulo']='Adicionar Equipamento';
      $data['jscript']='theme/footer';
      $data['secretaria'] = $this->Mfuncionario->get('tb_secretaria', '*', $where = '', $config['per_page']=null, null);
      //carregar os modelos dos equipamentos e categoria
      $data['mequipamento'] = $this->MMequipamento->get('tb_marc_equip', '*', $where = '', $config['per_page']=null, null);
      $data['cequipamento'] = $this->MCequipamento->get('tb_cat_equip', '*', $where = '', $config['per_page']=null, null);
      $data['view']='admin/equipamento/add';//passa a view por padrao
      $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
      $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
       $this->load->view('theme/header',$data);

    }
    public function save(){
       /*
       ["n_patrimonio"]=> string(10) "patrimonio" 
       ["nm_equipamento"]=> string(18) "nome do patrimonio" 
       ["desc_equipamento"]=> string(10) "descripadf" 
       ["nm_rede"]=> string(9) "nome rede" 
       ["n_mac"]=> string(3) "mac" 
       ["cid_secretaria"]=> string(1) "8" 
       ["id_setor"]=> string(1) "3" */
        $this->form_validation->set_rules('n_patrimonio', 'Patrimônio', 'required');
        $this->form_validation->set_rules('nm_equipamento', 'Nome', 'required');

        if ($this->form_validation->run()) {
            //if this check of validation ok, to do  this action
            
            //pega os dados vindos do post
           
            $n_patrimonio = $this->input->post('n_patrimonio');
            $nm_equipamento = $this->input->post('nm_equipamento');
            $desc_equipamento = $this->input->post('desc_equipamento');
            $nm_rede = $this->input->post('nm_rede');
            $n_mac = $this->input->post('n_mac');
            $cid_secretaria = $this->input->post('cid_secretaria');
            $cid_setor = $this->input->post('id_setor');
            //monta o array de dados
            $dados=array(
                'n_patrimonio'=>$n_patrimonio,
                'nm_equipamento'=>$nm_equipamento,
                'nm_rede'=>$nm_rede,
                'desc_equipamento'=>$desc_equipamento,
                'n_mac'=>$n_mac,
               // 'cid_secretaria'=>$cid_secretaria, #secretaria não precisa aqui.
                'cid_setor'=>$cid_setor
            );
            
            
            if ($this->Mequipamento->verificaUnicidade('tb_equipamento','n_patrimonio',$where=['n_patrimonio'=>$n_patrimonio])) {
                 //Verifica se já existe esse dada cadastrado no banco de dados
                 set_msg('salvo', '<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalSuccess">Success</a>
                 <div id="modalSuccess" class="modal-block modal-block-warning mfp-hide">
										<section class="panel">
											<header class="panel-heading">
												<h2 class="panel-title">ERPOS</h2>
											</header>
											<div class="panel-body">
												<div class="modal-wrapper">
													<div class="modal-icon">
														<i class="fa fa-warning"></i>
													</div>
													<div class="modal-text">
														<h4>Atenção</h4>
														<p>Esse equipamento já esta cadastrado.</p>
													</div>
												</div>
											</div>
											<footer class="panel-footer">
												<div class="row">
													<div class="col-md-12 text-right">
														<button class="btn btn-warning modal-dismiss">OK</button>
													</div>
												</div>
											</footer>
										</section>
									</div>', 'sucesso');
                redirect('equipamento/novo', 'refresh');
            } else {
                $this->Mequipamento->add('tb_equipamento', $dados);
                //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                 set_msg('salvo','<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalSuccess">Success</a>
                 <div id="modalSuccess" class="modal-block modal-block-success mfp-hide">
										<section class="panel">
											<header class="panel-heading">
												<h2 class="panel-title">ERPOS</h2>
											</header>
											<div class="panel-body">
												<div class="modal-wrapper">
													<div class="modal-icon">
														<i class="fa fa-check"></i>
													</div>
													<div class="modal-text">
														<h4>Sucesso!</h4>
														<p>Equipamento adicionado com sucesso.</p>
													</div>
												</div>
											</div>
											<footer class="panel-footer">
												<div class="row">
													<div class="col-md-12 text-right">
														<button class="btn btn-success modal-dismiss">OK</button>
													</div>
												</div>
                                            </footer></div> ','sucesso');
                
                redirect('equipamento', 'refresh');
                
                /*  set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                   Cargo adicionado com sucesso.
                 </div>', 'sucesso');
                  redirect('cargo', 'refresh'); */
            }
              
            
            
        } else {
            //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.
           $data['titulo']='Editar Equipamento';
      $data['view']='admin/equipamento/edit';//passa a view por padrao
      $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
      $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
      $data['jscript']='theme/footer';
      $data['secretaria'] = $this->Mfuncionario->get('tb_secretaria', '*', $where = '', $config['per_page']=null, null);
      $data['view']='admin/equipamento/add';//passa a view por padrao
      $this->load->view('theme/header',$data);
         
        }
    }
    public function edit(){
      //pega parametro da url via get
      $id = decrypt($this->uri->segment(3));
      //primeira coisa fazer um select com id
      $config['per_page']=null;
      $data['equipamento'] = $this->Mequipamento->get('v_equipamento', '*', $where =['id_equipamento'=>$id], $config['per_page'], null);
      
      $data['titulo']='Editar Equipamento';
      $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
      $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
      $data['jscript']='theme/footer';
      $data['secretaria'] = $this->Mfuncionario->get('tb_secretaria', '*', $where = '', $config['per_page']=null, null);
      $data['view']='admin/equipamento/edit';//passa a view por padrao
      $this->load->view('theme/header',$data);
    }

    public function update(){
     $id=$this->input->post('id_equipamento');
    
     
     //editar  meio parecido com save
       $this->form_validation->set_rules('n_patrimonio', 'Patrimônio', 'required');

     if ($this->form_validation->run()) {
         //if this check of validation ok, to do  this action 
         
       //pega os dados vindos do post
           
            $n_patrimonio = $this->input->post('n_patrimonio');
            $nm_equipamento = $this->input->post('nm_equipamento');
            $desc_equipamento = $this->input->post('desc_equipamento');
            $nm_rede = $this->input->post('nm_rede');
            $n_mac = $this->input->post('n_mac');
            $cid_secretaria = $this->input->post('cid_secretaria');
            $cid_setor = $this->input->post('id_setor');
            //monta o array de dados
            $dados=array(
                'n_patrimonio'=>$n_patrimonio,
                'nm_equipamento'=>$nm_equipamento,
                'nm_rede'=>$nm_rede,
                'desc_equipamento'=>$desc_equipamento,
                'n_mac'=>$n_mac,
               // 'cid_secretaria'=>$cid_secretaria, #secretaria não precisa aqui.
                'cid_setor'=>$cid_setor
            );
         
         if ($this->Mequipamento->verificaUnicidade('v_equipamento','n_patrimonio',$where=['n_patrimonio'=>$patrimonio])) {
              //Verifica se já existe esse dada cadastrado no banco de dados
             set_msg('salvo', '<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalSuccess">Success</a><div id="modalWarning" class="modal-block modal-block-warning mfp-hide">
             <section class="panel">
                 <header class="panel-heading">
                     <h2 class="panel-title">ERPOS</h2>
                 </header>
                 <div class="panel-body">
                     <div class="modal-wrapper">
                         <div class="modal-icon">
                             <i class="fa fa-warning"></i>
                         </div>
                         <div class="modal-text">
                             <h4>Atenção!</h4>
                             <p>Esse equipamento já esta cadastrado.</p>
                         </div>
                     </div>
                 </div>
                 <footer class="panel-footer">
                     <div class="row">
                         <div class="col-md-12 text-right">
                             <button class="btn btn-warning modal-dismiss">OK</button>
                         </div>
                     </div>
                 </footer>
             </section>
         </div>', 'sucesso');
             redirect('equipamento/novo', 'refresh');
         } else {
             $this->Mequipamento->edit('tb_equipamento',$dados,'id_equipamento',$id);
             //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
             set_msg('salvo','<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalSuccess">Success</a>
                 <div id="modalSuccess" class="modal-block modal-block-success mfp-hide">
										<section class="panel">
											<header class="panel-heading">
												<h2 class="panel-title">ERPOS</h2>
											</header>
											<div class="panel-body">
												<div class="modal-wrapper">
													<div class="modal-icon">
														<i class="fa fa-check"></i>
													</div>
													<div class="modal-text">
														<h4>Sucesso!</h4>
														<p>Equipamento  Atualizado.</p>
													</div>
												</div>
											</div>
											<footer class="panel-footer">
												<div class="row">
													<div class="col-md-12 text-right">
														<button class="btn btn-success modal-dismiss">OK</button>
													</div>
												</div>
                                            </footer></div> ','sucesso');
                
                redirect('equipamento', 'refresh');  
             
             
            
         }
           
         
      
     } else {
         //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.
         exit;
         $data['equipamento'] = $this->Mequipamento->get('v_equipamento', '*', $where =['id_equipamento'=>$id], $config['per_page'], null);
      
      $data['titulo']='Editar Equipamento';
      $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
      $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
      $data['jscript']='theme/footer';
      $data['secretaria'] = $this->Mfuncionario->get('tb_secretaria', '*', $where = '', $config['per_page']=null, null);
      $data['view']='admin/equipamento/edit';//passa a view por padrao
      $this->load->view('theme/header',$data);
     }  

        

    }  
    public function delete(){
     $id=decrypt($this->uri->segment(3));
     //deletar simples sem confirmação por hora
     
      
     try {
       //senão tiver executa a exclusão.
     $this->Mequipamento->delete('tb_equipamento','id_equipamento',$id);
     
     
     set_msg('salvo','<a class="mb-xs mt-xs hide mr-xs modal-basic btn btn-success" href="#modalSuccess">Success</a><div id="modalSuccess" class="modal-block modal-block-info mfp-hide">
     <section class="panel">
         <header class="panel-heading">
             <h2 class="panel-title">ERPOS</h2>
         </header>
         <div class="panel-body">
             <div class="modal-wrapper">
                 <div class="modal-icon">
                     <i class="fa fa-info-circle"></i>
                 </div>
                 <div class="modal-text">
                     <h4>Informação</h4>
                     <p>Registro apagado.</p>
                 </div>
             </div>
         </div>
         <footer class="panel-footer">
             <div class="row">
                 <div class="col-md-12 text-right">
                     <button class="btn btn-info modal-dismiss">OK</button>
                 </div>
             </div>
         </footer>
     </section>
 </div>','sucesso');
 redirect('equipamento', 'refresh'); 
     
     /*set_msg('salvo', '<div class="alert alert-info alert-dismissible">
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
     <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
     Registro apagado.
   </div>', 'sucesso');
    redirect('cargo', 'refresh'); */
    } catch (Exception $e) {
        echo 'Exceção capturada: ',  $e->getMessage(), "\n";
    }
  

    
    }
    
    
    
    
}


