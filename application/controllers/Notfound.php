<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notfound extends CI_Controller {

	/**
	 * PAGINA DE ERRO 404
	 * SEMPRE QUE O USUARIO TENTAR ACESSAR UMA URL INEXISTENTE OU PARAMETRO INCORRETO VEM PARAR AQUI
	 * MUDAR O LAYOUT.
	 * WESLEY
	 */
	public function index()
	{
		$this->load->view('theme/header');
		$this->load->view('theme/erros/404');
		$this->load->view('theme/footer');

}
}
