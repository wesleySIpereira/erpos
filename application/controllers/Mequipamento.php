<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mequipamento extends CI_Controller {

	/**
	 * 
	 */
    public function __construct() {
        parent::__construct();
        
        $this->load->model('mequipamento/Mequipamento_model','MMequipamento');
        $this->load->library('form_validation');
        if(!$this->session->userdata('id_funcionario')){
      
			
			set_msg('salvo', '<center><i class="fa fa-warning text-danger"></i> <label class="text-danger text-center">Você precisa estar logado para acessar.</label></center>', 'sucesso');
			redirect('principal', 'refresh');
			 
		   }
    }
    public function index()
	{
        //index manda para funcão principal gerenciar
      $this->gerenciar();
        
    }
    public function gerenciar(){

          //filtro nova os
          /*
        
        */
        $filtro=false;   
        //verifica se existe condição de busca #endregion
     if(isset($_GET['nm_marc_equip']) && !empty($_GET['nm_marc_equip'])){   
        $url='';
        
        if(isset($_GET['nm_marc_equip']) && !empty($_GET['nm_marc_equip'])){
            $filtro=true;
            $url='&nm_marc_equip='.$_GET['nm_marc_equip'];
            $like['nm_marc_equip']=$_GET['nm_marc_equip'];  
        }
        
       
       
    }else{
        $like=null;
        $url='';
    }
    
        //primeiro passas as  configuraçoes para pagination

     //$data['secretaria'] = $this->Mfuncionario->get('tb_secretaria', '*', $where = '',$join='', $config['per_page']=null, $this->uri->segment(3));
     $p=0;//inicio do contador da paginação
     $pg=1;
     $total_registros= count($this->MMequipamento->listaCargoLike($p=0,$por_pagina=null,'tb_marc_equip','*',$like,$order=null));//pegar total registros
     $per_page=5;//numero de registros por paginas;
     $paginas=$total_registros/$per_page;
       
     if(isset($_GET['p']) && !empty($_GET['p'])){
        $pg=addslashes($_GET['p']);
    }
    $p=($pg-1)*$per_page;
       
 


        //primeiro busca todos os dados no banco 
        $data['filtro']=$filtro;
        $data['mequipamento'] = $this->MMequipamento->listaCargoLike($p,$per_page,'tb_marc_equip','*',$like,$order=null);
        $data['pag']=paginacao($total_registros,$per_page,$pg,$url);
        $data['titulo']='Marca  Equipamento'; //titulo da pagina
        $data['view']='admin/mequipamento/index';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);

    }
    public function novo(){
       //função que chama o formulario para adição de novo categoria
        
        $data['titulo']='Marca  Equipamento'; //titulo da pagina
        $data['view']='admin/mequipamento/add';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);
    }
    public function save(){
        
        $this->form_validation->set_rules('nm_marc_equip', 'Nome', 'required');

        if ($this->form_validation->run()) {
            //if this check of validation ok, to do  this action
            
           
            $nome = $this->input->post('nm_marc_equip');
           
            //monta o array de dados
            $dados=array(
                'nm_marc_equip'=>$nome
               
            );
            
            if ($this->MMequipamento->verificaUnicidade('tb_marc_equip','nm_marc_equip',$where=['nm_marc_equip'=>$nome])) {
                 //Verifica se já existe esse dada cadastrado no banco de dados
                 set_msg('salvo', '<div class="alert alert-warning alert-dismissible">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                 <h4><i class="icon fa fa-check"></i> Atenção!</h4>
                 Esse categoria já esta cadastrado.
               </div>', 'sucesso');
                redirect('mequipamento/novo', 'refresh');
            } else {
                $this->MMequipamento->add('tb_marc_equip', $dados);
                //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                   set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                   Categoria adicionada com sucesso.
                 </div>', 'sucesso');
                  redirect('mequipamento', 'refresh');
            }
              
            
            
        } else {
            //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.
           
        $data['titulo']='Novo Tipo Equipamento'; //titulo da pagina
        $data['view']='admin/mequipamento/add';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);
        }
    }
    public function edit(){
      //pega parametro da url via get
      $id = decrypt($this->uri->segment(3));
      //primeira coisa fazer um select com id
      $config['per_page']=null;
      $data['mequipamento'] = $this->MMequipamento->get('tb_marc_equip', '*', $where =['id_marc_equip'=>$id], $config['per_page'], null);
      
     
        $data['titulo']='Editar Marca Equipamento'; //titulo da pagina
        $data['view']='admin/mequipamento/edit';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);

    }

    public function update(){
     $id=$this->input->post('id_marc_equip');
     $nome=$this->input->post('nm_marc_equip'); 
     
     //editar  meio parecido com save
     $this->form_validation->set_rules('nm_marc_equip', 'Nome', 'required');

     if ($this->form_validation->run()) {
         //if this check of validation ok, to do  this action
         
        
         $nome = $this->input->post('nm_marc_equip');
        
         //monta o array de dados
         $dados=array(
             'nm_marc_equip'=>$nome
             
         );
         
         if ($this->MMequipamento->verificaUnicidade('tb_marc_equip','nm_marc_equip',$where=['nm_marc_equip'=>$nome])) {
              //Verifica se já existe esse dada cadastrado no banco de dados
              set_msg('salvo', '<div class="alert alert-warning alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4><i class="icon fa fa-check"></i> Atenção!</h4>
              Esse categoria já esta cadastrado.
            </div>', 'sucesso');
             redirect('mequipamento/novo', 'refresh');
         } else {
             $this->MMequipamento->edit('tb_marc_equip',$dados,'id_marc_equip',$id);
             //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                categoria Atualizado.
              </div>', 'sucesso');
               redirect('mequipamento', 'refresh');
         }
           
         
         
     } else {
         //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.
        
        $data['titulo']='Editar Categoria'; //titulo da pagina
        $data['view']='admin/categoria/edit';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);
     }



    }
    public function delete(){
     $id=decrypt($this->uri->segment(3));
     //deletar simples sem confirmação por hora
     
      
     try {
       //senão tiver executa a exclusão.
     $this->MMequipamento->delete('tb_marc_equip','id_marc_equip',$id);
     set_msg('salvo', '<div class="alert alert-info alert-dismissible">
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
     <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
     Registro apagado.
   </div>', 'sucesso');
    redirect('mequipamento', 'refresh');
    } catch (Exception $e) {
        echo 'Exceção capturada: ',  $e->getMessage(), "\n";
    }
  

    
    }
    
    public function get_categoria()
    {  
        //$id_setor = $this->input->post('cid_secretaria');
       
        echo $this->MMequipamento->geraSelect('tb_subcategoria', $where = ['cid_marc_equip' =>$this->input->post('cid_secretaria')]);

    } 
    
    
}

?>