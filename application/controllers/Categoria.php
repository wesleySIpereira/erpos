<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends CI_Controller {

	/**
	 * 
	 */
    public function __construct() {
        parent::__construct();
        
        $this->load->model('categoria/Categoria_model','MCategoria');
        $this->load->library('form_validation');
        if(!$this->session->userdata('id_funcionario')){
      
			
			set_msg('salvo', '<center><i class="fa fa-warning text-danger"></i> <label class="text-danger text-center">Você precisa estar logado para acessar.</label></center>', 'sucesso');
			redirect('principal', 'refresh');
			 
		   }
    }
    public function index()
	{
        //index manda para funcão principal gerenciar
      $this->gerenciar();
        
    }
    public function gerenciar(){

          //filtro nova os
          /*
        
        */
        $filtro=false;   
        //verifica se existe condição de busca #endregion
     if(isset($_GET['nm_categoria']) && !empty($_GET['nm_categoria'])){   
        $url='';
        
        if(isset($_GET['nm_categoria']) && !empty($_GET['nm_categoria'])){
            $filtro=true;
            $url='&nm_categoria='.$_GET['nm_categoria'];
            $like['nm_categoria']=$_GET['nm_categoria'];  
        }
        
       
       
    }else{
        $like=null;
        $url='';
    }
    
        //primeiro passas as  configuraçoes para pagination

     //$data['secretaria'] = $this->Mfuncionario->get('tb_secretaria', '*', $where = '',$join='', $config['per_page']=null, $this->uri->segment(3));
     $p=0;//inicio do contador da paginação
     $pg=1;
     $total_registros= count($this->MCategoria->listaCargoLike($p=0,$por_pagina=null,'tb_categoria','*',$like,$order=null));//pegar total registros
     $per_page=5;//numero de registros por paginas;
     $paginas=$total_registros/$per_page;
       
     if(isset($_GET['p']) && !empty($_GET['p'])){
        $pg=addslashes($_GET['p']);
    }
    $p=($pg-1)*$per_page;
       
 


        //primeiro busca todos os dados no banco 
        $data['filtro']=$filtro;
        $data['categoria'] = $this->MCategoria->listaCargoLike($p,$per_page,'tb_categoria','*',$like,$order=null);
        $data['pag']=paginacao($total_registros,$per_page,$pg,$url);
        $data['titulo']='Categoria'; //titulo da pagina
        $data['view']='admin/categoria/index';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);

    }
    public function novo(){
       //função que chama o formulario para adição de novo categoria
        $data['filtro']=$filtro;
        $data['titulo']='Adicionar Categoria'; //titulo da pagina
        $data['view']='admin/categoria/add';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);
    }
    public function save(){
        
        $this->form_validation->set_rules('nome', 'Nome', 'required');

        if ($this->form_validation->run()) {
            //if this check of validation ok, to do  this action
            
           
            $nome = $this->input->post('nome');
           
            //monta o array de dados
            $dados=array(
                'nm_categoria'=>$nome
               
            );
            
            if ($this->MCategoria->verificaUnicidade('tb_categoria','nm_categoria',$where=['nm_categoria'=>$nome])) {
                 //Verifica se já existe esse dada cadastrado no banco de dados
                 set_msg('salvo', '<div class="alert alert-warning alert-dismissible">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                 <h4><i class="icon fa fa-check"></i> Atenção!</h4>
                 Esse categoria já esta cadastrado.
               </div>', 'sucesso');
                redirect('categoria/novo', 'refresh');
            } else {
                $this->MCategoria->add('tb_categoria', $dados);
                //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                   set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                   Categoria adicionada com sucesso.
                 </div>', 'sucesso');
                  redirect('categoria', 'refresh');
            }
              
            
            
        } else {
            //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.
           
           $data['filtro']=$filtro;
        $data['titulo']='Adicionar Categoria'; //titulo da pagina
        $data['view']='admin/categoria/add';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);
        }
    }
    public function edit(){
      //pega parametro da url via get
      $id = decrypt($this->uri->segment(3));
      //primeira coisa fazer um select com id
      $config['per_page']=null;
      $data['categoria'] = $this->MCategoria->get('tb_categoria', '*', $where =['id_categoria'=>$id], $config['per_page'], null);
      
     
        $data['titulo']='Editar Categoria'; //titulo da pagina
        $data['view']='admin/categoria/edit';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);

    }

    public function update(){
     $id=$this->input->post('id_categoria');
     $nome=$this->input->post('nm_categoria'); 
     
     //editar  meio parecido com save
     $this->form_validation->set_rules('nm_categoria', 'Nome', 'required');

     if ($this->form_validation->run()) {
         //if this check of validation ok, to do  this action
         
        
         $nome = $this->input->post('nm_categoria');
        
         //monta o array de dados
         $dados=array(
             'nm_categoria'=>$nome
             
         );
         
         if ($this->MCategoria->verificaUnicidade('tb_categoria','nm_categoria',$where=['nm_categoria'=>$nome])) {
              //Verifica se já existe esse dada cadastrado no banco de dados
              set_msg('salvo', '<div class="alert alert-warning alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4><i class="icon fa fa-check"></i> Atenção!</h4>
              Esse categoria já esta cadastrado.
            </div>', 'sucesso');
             redirect('categoria/novo', 'refresh');
         } else {
             $this->MCategoria->edit('tb_categoria',$dados,'id_categoria',$id);
             //verifica se deu tudo ok na criação se sim retorna p/ pagina e seta uma flash data mg
                set_msg('salvo', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                categoria Atualizado.
              </div>', 'sucesso');
               redirect('categoria', 'refresh');
         }
           
         
         
     } else {
         //se der erro de validação nos campos cai aqui e mostra o erro p/ usuário.
        
        $data['titulo']='Editar Categoria'; //titulo da pagina
        $data['view']='admin/categoria/edit';//passa a view por padrao
        $data['usuario']=$this->session->userdata('nm_funcionario');//passa nome do usuario 
        $data['cargo']=$this->session->userdata('nm_cargo');//passa o nome do cargo
        $this->load->view('theme/header',$data);
     }



    }
    public function delete(){
     $id=decrypt($this->uri->segment(3));
     //deletar simples sem confirmação por hora
     
      
     try {
       //senão tiver executa a exclusão.
     $this->MCategoria->delete('tb_categoria','id_categoria',$id);
     set_msg('salvo', '<div class="alert alert-info alert-dismissible">
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
     <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
     Registro apagado.
   </div>', 'sucesso');
    redirect('categoria', 'refresh');
    } catch (Exception $e) {
        echo 'Exceção capturada: ',  $e->getMessage(), "\n";
    }
  

    
    }
    
    public function get_categoria()
    {  
        //$id_setor = $this->input->post('cid_secretaria');
       
        echo $this->MCategoria->geraSelect('tb_subcategoria', $where = ['cid_categoria' =>$this->input->post('cid_secretaria')]);

    } 
    
    
}

?>